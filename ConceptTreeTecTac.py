
from math import sqrt
import json
import copy
import re

# For python 3.10

Version = "2.1"

Novelty_Types = ['Total', 'Technology', 'Tactics']

class ConceptTree():
    """A tree of Concept Nodes, with multiple Levels, that can include both technology and tactics"""
    def __init__(self, id, beta = 0.7, quality_threshold = 0.25):
        self.id = id
        self.max_level = 0
        self.nodes = {}
        self.beta = beta
        self.quality_threshold = 0.25
        self.version = Version
        self.novelty = {}
        self.variety = {}
        for n in Novelty_Types:
            self.novelty[n] = 0.0
            self.variety[n] = 0.0
 
    def add_node(self,node):
        """Add node to tree"""
        self.nodes[node.id] = node
        self.calc_levels()
        self.set_content()
        self.calc_novelty('All') # variety calculation is at start of novelty calc
        return node
        
    def remove_node(self,node):
        """Remove node from tree"""
        for n in self.nodes.values():
            if n != node:   #check for relationships
                if node in n.children.values(): # found a child relationship
                    del n.children[node.id]
                if n.parent == node: #found a parent relationship
                    n.parent = None
        del self.nodes[node.id]
        self.calc_levels()
        self.set_content()
        self.calc_novelty('All')
        
    def list_nodes(self):
        """List the nodes in the tree"""
        list(self.nodes)

    def save_file(self, filename):
        """ Save the tree in a JSON file """
        f=open(filename,'w')
        f.write(json.dumps(self.export_json()))
        f.close

    def load_file(self, filename):
        """ Create a new concept tree from the data stored in a JSON file"""
        f=open(filename, 'r')
        j = f.readlines()[0]
        return self.import_json(j)

# Mildly deprecated
    def save_latex_novelty_standalone(self):
        """ Save LaTeX diagram of the Novelty as standalone image"""
        output_file = self.id + "Novelty.tex"
        of = open(output_file, 'w')
        self.export_space_rep(of,'Novelty')
        of.close

    def save_latex_novel_design_space_standalone(self):
        """ Save LaTeX diagram of the novel design space as standalone image"""
        output_file = self.id + "NovelDesignSpace.tex"
        of = open(output_file, 'w')
        self.export_space_rep(of,'NDSI')
        of.close

    def save_latex_design_space_standalone(self):
        """ Save LaTeX diagram of the design space as standalone image"""
        output_file = self.id + "DesignSpace.tex"
        of = open(output_file, 'w')
        self.export_space_rep(of,'DSI')
        of.close
    # End of deprecated LaTeX output

    
    def export_json(self):
        """Export the nodes in the tree in JSON format"""
        #TODO:  Add technology and tactics to  nodes, variety, and novelty
        #TODO:  Make JSON export a function of nodes (in node class); export only tree relationships here
        n_dict = {}
        for n in self.nodes.keys():
            sn = self.nodes[n]
            if sn.children == {}:
                novelty = self.nodes[n].c_novelty
            else:
                novelty = 0
            p = sn.parent
            if p != None:
                p = p.id
            n_dict[n]= { 'parent':p, 'label':sn.node_label,'positioning':sn.positioning, 'novelty':novelty,
                         'color':sn.fillcolor, 'fixed_level':sn.fixed_level}
        return { 'id':self.id, 'max_level':self.max_level, 'beta':self.beta, 'nodes': n_dict }
            
    def import_json(self, str):
        """Import a tree from a JSON string"""
        #TODO: Add technology and tactics to nodes, variety, and novelty
        #TODO: Add a json import for the nodes (in node class) (handled separately here)
        j = json.loads(str)
        t = ConceptTree(j['id'])
        # first, add all the nodes
        for nid in j['nodes'].keys():
            t.add_node(ConceptNode(nid))
        # now, add other values
        for nid in j['nodes'].keys():
            jn = j['nodes'][nid]
            tn = t.nodes[nid]
            # add parent
            if jn['parent'] != None:
                pid = jn['parent']
                p = t.nodes[pid]
                tn.set_parent(p)
            # add auxiliary data
            tn.node_label = jn['label']
            tn.positioning = jn['positioning']
            tn.c_novelty = jn['novelty']
            tn.fillcolor = jn['color']
            tn.fixed_level = jn['fixed_level']
        # calculate the levels,                             
        t.calc_levels()
        t.beta = j['beta']
        t.calc_novelty('All')
        return t
        
        

    def display_tree(self, kind = 'All'):
        """Make a text display of a tree, with children indented"""
        t_nodes = self.top_nodes()
        for n in t_nodes:
            n.display_subtree(self.max_level, kind)

        
# Mildly deprecated -- we're not using space reps right now
    def export_space_rep(self, f_latex, s_kind):
        # Write the preamble
        print ( "\\documentclass {standalone}\n"\
                "\\usepackage{tikz}\n"\
                "\\usetikzlibrary{positioning}\n"\
                "\\begin{document}\n"\
                "\\begin{tikzpicture}\n"\
                "\\scriptsize",\
                file = f_latex)
        # Write the nodes
        print (self.create_ds_representation(s_kind), file = f_latex)
        # End the document
        print ( "\\end{tikzpicture}\n"\
                "\\end{document}",\
                file = f_latex)

# Mildly deprecated        
    def create_ds_representation (self, kind="DSI"):
        """ Create a design space representation for a tree.  kind can be 'DSI', 'NDSI', or 'Novelty'.
Returns a string that kan be used with TikZ to create the representation in a pdf file."""
        xmin = 0
        dsr = ""
        for n in self.top_nodes():
            n_rep = n.design_space_representation (kind, xmin)
            xmin += n_rep[0]
            dsr += n_rep[1]
        return (dsr)
        

       
#TODO: Figure out how to handle parent-child relationships including partial trees
    def calc_levels(self):
        """Calculate the tree levels of all nodes in the tree, and set the maximum level"""
        t_nodes = self.top_nodes()
        # find the level based on the children
        #for n in t_nodes:
            #if len(n.children.values()) == 0:
                #if n.level == 0 :
 #                   n.level = 1 # default value
            #if len(n.children.values()) > 0:
 #               n.level = 1 + max(c.child_level() for c in n.children.values())
            #if n.fixed_level != 0:
 #               n.level = n.fixed_level
        # adjust as needed for parent levels
        #for n in t_nodes:
            #for c in n.children.values():
                #c.parent_level()
        for n in t_nodes:
            n.level = n.fixed_level
            cv = n.children.values()
            if len(cv) > 0:
                for c in cv:
                    c.child_level()
        self.max_level = max(n.level for n in t_nodes)
           
    def top_nodes(self):
        """Return a list containing the nodes in the tree with no parents"""
        t_nodes = []
        for n in self.nodes.values():
            if n.parent == None:
                t_nodes.append(n)
        return t_nodes


    def calc_row_positions(self):
        """Calculate the desired row positions for every node in the tree"""
        for n in self.nodes.values():
            self.row=-1
            self.child_span = [-1, -1]
        first_row = 1
        for t in self.top_nodes():
            last_row = t.calc_row_pos(first_row)
            first_row = last_row + 1

    def calc_quantity(self, kind):
        """Caclulate the quantity of ideas in the tree (the number of leaves)"""
        quantity = 0
        for n in self.nodes.values():
            if n.has_content[kind] and n.child_count(kind) == 0:
                quantity += 1
        return(quantity)
                
    def set_content(self):
        """ Set the appropriate values for has_content based on children """
        # First, clear content for all nodes with children (needed when child is deleted)
        # Question: Do we need to leave Total alone?  I think it will work fine as-written
        for n in self.nodes.values():
            #print ('Debug in set_content')
            #print ('Node id: {} content:{}'.format (\
            #    n.id, n.has_content))
            ch = n.children # for debugging
            if n.children != {}:
                for t in Novelty_Types:
                    n.has_content[t] = False
        # Now set content for all parents
        for l in range(1,5):
            for n in self.nodes.values():
                if n.level == l:
                    if n.parent != None:
                        #print ('Debug: Parent: {}'.format(n.parent.id))
                        #print (n.parent.has_content)
                        #print (n.has_content)
                        for t in Novelty_Types:
                            if n.has_content[t]:
                                n.parent.has_content[t] = True

    def calc_variety(self, kind='All'):
        """Perform variety calculations on the tree"""
        if kind == 'All':
            for k in Novelty_Types:
                self.calc_specific_variety(k)
            return self.variety['Total'] # This might be wrong
        else:
            self.calc_specific_variety(kind)
        return self.variety[kind]

    def calc_specific_variety(self, kind):
        total_variety = 0
        for t in self.top_nodes():
            t.branch_variety(self.beta, kind)
            total_variety += t.b_variety[kind]
        #for t in self.top_nodes():
        #    t.allocated_variety()
        self.variety[kind] = total_variety
        return(total_variety)

    def minimum_variety(self, kind = 'Total'):
        """Calculate the minimum variety possible in a tree with the same number
           and kinds of nodes"""
        element_count = [0, 0, 0, 0]
        min_v = 0
        for n in self.nodes.values():
            if n.has_content[kind]:
                element_count[n.level-1]+=1
        for i in range(0,4):
            min_v += ConceptNode.level_weighting(i+1) * \
                     ConceptNode.average_variety(element_count[i],self.beta) * \
                     element_count[i]
        return(min_v)    

    def maximum_variety(self, kind = 'Total'):
        """Calculate the maximum variety possible in a tree with the same number
           and kinds of nodes"""
        element_count = [0, 0, 0, 0]
        f_max = [0, 0, 0, 0]
        n_max = [0, 0, 0, 0]
        s_max = [0, 0, 0, 0]
        d_max = [0, 0, 0, 0]
        b_max = [0, 0, 0, 0]
        min_v = 0
        for n in self.nodes.values():
            if n.has_content[kind]:
                element_count[n.level-1]+=1
        f_max[3]=element_count[3]
        n_max[3] = 1
        for i in range(3, 0, -1):
            k = i-1
            f_max[k] = element_count[k+1]
            n_max[k] = element_count[k]/element_count[k+1]
            s_max[k] = ConceptNode.average_variety(n_max[k],self.beta)
        b_max[0] = ConceptNode.level_weighting(1)*s_max[0]
        for k in range(1,4):
            d_max[k] = n_max[k-1]*b_max[k-1]  
            b_max[k] = ConceptNode.level_weighting(k+1)*s_max[k]+d_max[k]
        return b_max[3]
        

    def calc_novelty(self, kind = 'All'):
        """Perform novelty calculations on the tree"""
        if kind == 'All':
            for k in Novelty_Types:
                self.calc_variety(k)
                nov = self.novelty_calculations(k)
                self.novelty[k] = nov
                #debug
                #print ('in calc_novelty, {} novelty {:.2f}\n'.format(k, self.novelty[k]))

        else:
            nov = self.novelty_calculations(kind)
            self.novelty[kind] = nov

    def novelty_calculations(self, kind):
        self.calc_variety(kind)
        total_novelty = 0
        for t in self.top_nodes():
            t.calc_novelty(kind) #first get the concept novelty
            t.calc_set_novelty(self.beta,kind) # This is a recursive function over the whole branch
            total_novelty += t.sb_novelty[kind]
        return(total_novelty)

    def calc_quality(self, kind):
        """Calculate the set quality based on the idea quality"""
        set_quality = 0
        q_thresh = self.quality_threshold # minimum quality level to count for a quality idea
        for n in self.nodes.values():
            if n.has_content[kind] and n.quality >= q_thresh:
                set_quality += n.quality - q_thresh
        return(set_quality)

    def save_dot_tree(self, twidth = 20, sdist = 0.5, rankdist = 1.5, plain = False, kind = 'All'): 
        """ Save graphviz (dot) node descriptions to create a graphical representation of a tree"""
        output_file = self.id + kind + ".dot"
        of = open(output_file, 'w')
        self.export_dot_tree(of, twidth, sdist, rankdist, plain, kind)
        of.close

    def save_dot_tree_plain(self, twidth = 20, sdist = 0.5, rankdist = 1.5): 
        """ Save graphviz (dot) node descriptions to create a graphical representation of a tree"""
        output_file = self.id + "Plain.dot"
        of = open(output_file, 'w')
        self.export_dot_tree(of, twidth, sdist, rankdist, True, 'All')
        of.close


    def export_dot_tree (self, f_dot, twidth, sdist, rankdist, plain = False, kind = 'All'):
        """Export the OPED Tree in dot file import mode"""
        # Write the preamble
        preamble = "strict graph {{\n" \
                   'rankdir = "LR"\n' \
                   'splines = true\n' \
                   'ranksep = {}\n' \
                   'nodesep = {}'.format(rankdist, sdist)
        print ( preamble, file = f_dot)
        # Export the nodes
        self.export_dot_nodes (f_dot, twidth, plain=plain, kind=kind)
        # Write the conclusion
        print ( "}",file = f_dot)

    def export_dot_nodes(self, f_dot, twidth, plain = False, kind = 'All'):
        """Export the OPED Tree nodes"""
        node_dict = self.nodes
        #First write the nodes
        for n in node_dict.values():
            n_node = n.dot_node(twidth, plain, kind)
            f_dot.write("{}\n".format(n_node))
        
        #Write child relationships for each course in level order
        for l in range(self.max_level, 0, -1):
            for n in node_dict.values():
                if n.level == l:
                    child_string = n.dot_children()
                    f_dot.write(child_string)

        #Write in the non-child group links
        for n in node_dict.values():
            group_link = n.dot_group_link(self)
            if group_link != 'not needed' and group_link != 'not found':
                f_dot.write(group_link)

        # Write in the key
        #TODO: Make a node write function that lets the Key output use the same function
        #      as the regular nodes
        #      Note: we now have a node write function, but we don't use a key; we have labels in the boxes
        #  DEPRECATED -- we don't need this unless we eliminate labels.
        #if plain == False:
        #    Key = ConceptNode('Key')
        #    Key.row = 1
        #    KeyChild = ConceptNode('Key Child') # Dummy node to make alignment work
        #    Key.children[KeyChild.id]=KeyChild
        #    wrapped_title = ConceptNode.dot_wrapped_string('Element Name', twidth)
            #print (wrapped_title + ' ' + str(twidth) + ' ' + str(len(wrapped_title)))
        #    f_string = \
        #                '"Key" [shape=none, margin = 0, group = "group1", label=<\n' \
        #                '<TABLE BORDER = "0" CELLBORDER = "1" bgcolor = "white" cellspacing = "0">\n' \
        #                '<TR><TD colspan="4" border="0">Shape Key:</TD></TR>\n' \
        #                '<TR><TD>N</TD><TD>C<sub>i</sub></TD> <TD>S<sub>E</sub></TD> <TD>S<sub>D</sub></TD> </TR>\n' \
        #                '<TR><TD COLSPAN="4"><FONT POINT-SIZE="20">{}</FONT></TD></TR>\n' \
        #                '<TR><TD>w</TD> <TD>C<sub>A</sub></TD> <TD>V<sub>E</sub></TD> <TD>V<sub>D</sub></TD> </TR>\n'\
        #                '</TABLE>>]\n' 
        #    key_string = f_string.format(wrapped_title)
        #    f_dot.write(key_string)
        #    f_dot.write('{rank= "min"; "Key"}\n')
        #    invis_link = Key.dot_group_link(self)
        #    f_dot.write(invis_link + '\n')
                    
               
                    

    def display_nodes(self):
        """Display all the nodes in the tree"""
        for n in self.nodes.values():
            n.display()





#TODO: Reduce the property set to what is currently needed.
#      We currently have old calculated properties
#      Need to eliminate positioning and fillcolor (only used for LaTeX trees)
class ConceptNode():
    """A node to be placed in a concept tree"""
    def __init__(self, id, label="lab",parent = None, novelty = 0, positioning='',fill='', fixed_level=0):
        self.id = id
        self.node_label = label
        self.positioning = positioning #deprecated (used for LaTeX output)
        self.fillcolor = fill  #deprecated (used for LaTeX output)
        self.parent = None # id of parent node
        self.children = {} # dictionary of child nodes, with node id as keys
        self.fixed_level = fixed_level  # right now, we seem to use fixed levels
        self.level = fixed_level # deprecated?
        # properties having types
        self.b_variety = {} # branch variety
        self.e_variety = {} # element variety
        self.d_variety = {} # sum of descendant branch varieties
        self.c_average = {} # average contribution
        self.c_increment = {} # contribution increment
        self.b_increment = {} # total branch contribution increment
        self.c_novelty = {} # concept (idea) novelty
        self.sb_novelty = {} # set novelty for the branch
        self.se_novelty = {} # set novelty for the element
        self.sd_novelty = {} # sum of descendant set branch novelty
        self.space_width = {} # design space width for this element
        self.n_space_width = {} # novel design space width for this elemen
        self.d_n_space_width ={} # sum of descendant novel design space widths
        self.has_content = {} # does the idea contain technology or tactics
        # deprecated?
        for t in Novelty_Types:
            self.b_variety[t] = 0.0
            self.e_variety[t] = 0.0
            self.d_variety[t] = 0.0
            self.c_average[t] = 0.0
            self.c_increment[t] = 0.0
            self.b_increment[t] = 0.0
            self.c_novelty[t] = 0.0
            self.sb_novelty[t] = 0.0
            self.se_novelty[t] = 0.0
            self.sd_novelty[t] = 0.0
            #mildly deprecated
            self.space_width[t] = 0.0
            self.n_space_width[t] = 0.0
            self.d_n_space_width[t] = 0.0
            self.has_content[t] = False
        
        self.row = -1 # Row position of the node  
        self.child_span = [-1, -1] # row of first child, row of last child
        self.version = Version
        self.quality =  0. #idea quality
        self.feasibility  = 0. #idea feasibility
        self.effectiveness = 0. #idea effectiveness
        if parent != None:
            self.set_parent(parent)

    def set_concept_novelty(self, kind, novelty):
        """ Set the kind novelty for a concept.  If kind is 'Technology' or 'Tactics', """ \
            """ recalculate 'Total'"""
        self.c_novelty[kind] = novelty
        self.has_content[kind] = True
        if kind != 'Total':
            self.calc_total_novelty()
            self.has_content['Total'] = True

    def calc_total_novelty(self):
        """ Calculate the total novelty from the technology and tactics novelties """
        self.c_novelty['Total'] = sqrt(self.c_novelty['Technology']**2 + self.c_novelty['Tactics']**2)/ sqrt(2.0)
        #debug
        #print ('in calc_total_novelty: {}, Technology Novelty {:.2f} Tactics Novelty {:.2f} Total Novelty {:.2f}\n'\
        #       .format(self.id, self.c_novelty['Technology'], self.c_novelty['Tactics'], self.c_novelty['Total']))

    def child_count(self,kind):
        """Return the number of siblings of kind in the family of the node"""
        count = 0
        for c in self.children.values():
            if c.has_content[kind]:
                count += 1
        return(count)

    def calc_novelty(self,kind):
        """ Calculate the element kind novelty of a node """
        #print ("Debug: In calc_novelty: id: {}, kind: {}".format(self.id, kind))
        sid = self.id
        sum_child_novelty = 0.0
        sum_sq_child_novelty = 0.0
        if self.child_count(kind) == 0:
            #print ('Debug: self.c_novelty, no children')
            #print ('{}: {} c_novelty {:.2f}'.format (self.id, kind, self.c_novelty[kind]))
            if self.c_novelty[kind] == 0.0 :
                self.c_novelty[kind] = 0.0 # default value if none is given
            return (self.c_novelty[kind])
        else:
            #print ('Debug: self.c_novelty, with children')
            #print ('{}: {} c_novelty {:.2f}'.format (self.id, kind, self.c_novelty[kind]))
            self.has_content[kind] = True # we have children with this content
            novelty = 0.0
            for c in self.children.values():
                cnov = c.calc_novelty(kind)
                if cnov > novelty:
                    novelty = cnov
            self.c_novelty[kind] = novelty
            #print ('{}: {} c_novelty {:.2f}'.format (self.id, kind, self.c_novelty[kind]))
            return(self.c_novelty[kind])

    def calc_set_novelty(self, beta, kind):
        """Calculate the kind set novelty"""
        if self.has_content[kind] == False:
            self.c_novelty[kind] = 0.0
            self.sd_novelty[kind] = 0.0
            self.se_novelty[kind]= 0.0
            self.sb_novelty[kind] = 0.0
            for c in self.children.values():
                c.calc_set_novelty(beta, kind) #propagate the calculation down
            return (0.0)
        sum_child_set_novelty = 0.0
        sum_cb_inc = 0.0
        if self.child_count(kind) == 0:
            self.sd_novelty[kind] = 0.0
        else:
            sid = self.id
            for c in self.children.values():
                sum_child_set_novelty += c.calc_set_novelty(beta, kind) # recursive call
                sum_cb_inc += c.b_increment[kind]
            self.sd_novelty[kind] = sum_child_set_novelty
        if self.parent == None:
            self.se_novelty[kind] = self.c_novelty[kind] * self.e_variety[kind] # no parent, so no siblings, so VI=VE
        else:
            novelty_list = []
            for s in self.parent.children.values():
                if s.has_content[kind]:
                    novelty_list.append(s.c_novelty[kind])
            novelty_list.sort(reverse=True)
            sib_index = novelty_list.index(self.c_novelty[kind])
            self.c_increment[kind] = 1+beta ** sib_index
            i = sib_index + 1
            while i < len(novelty_list):
                if novelty_list[i] == self.c_novelty[kind]:
                    self.c_increment[kind] += 1 + beta ** i
                    i += 1
                else:
                    break
            self.c_increment[kind] /= (i-sib_index)
            weighted_inc = self.c_increment[kind] * self.weighting()
            self.b_increment[kind] = sum_cb_inc + weighted_inc 
            self.se_novelty[kind] = self.c_novelty[kind] * weighted_inc
        self.sb_novelty[kind] = self.se_novelty[kind] + self.sd_novelty[kind]
        return(self.sb_novelty[kind])

    def branch_variety(self,beta,kind):
        """Calculate and return the branch variety, including calculating
            descendant, element, and branch variety for all children."""
        self.d_variety[kind] = 0
        if self.parent == None:
            self.c_average[kind] = 2 # no siblings if no parent
        if self.has_content[kind] == False:
            self.c_average[kind] == 0 #not part of the variety calculation
        ch_sib_variety = self.children_variety(beta, kind)
        self.e_variety[kind] = self.c_average[kind] * self.weighting()
        for ch in self.children.values():
            if ch.has_content[kind]:
                ch.c_average[kind] = ch_sib_variety 
                self.d_variety[kind] += ch.branch_variety(beta,kind)
            else:
                ch.c_average[kind] = 0
        self.b_variety[kind] = self.d_variety[kind] + self.e_variety[kind]
        return(self.b_variety[kind])

# We no longer use allocated variety.  Space_width is now descendant variety + element variety = branch variety
# We could eliminate this method if we are sure we want to eliminate allocated variety.
# Note that space_width and b_variety are the same; we can eliminate space_width, I think.
    def allocated_variety(self):
        """ Calculate and return the parent and allocated variety for a node,
            and follow the tree down to calculate the parent and allocated variety
            for all children.  Depends on branch variety being correct.
            Also sets space_width."""
        if self.parent == None:
            self.p_variety = 0 # parent variety is obsolete; should we eliminate it?
        else:
            self.p_variety = self.parent.a_variety / self.parent.child_count()
        self.a_variety = self.p_variety + self.e_variety # allocated variety is obsolete; should we eliminate?  It's not wrong
        self.space_width = self.d_variety + self.e_variety
        for c in self.children.values():
            c.allocated_variety()
        return self.a_variety
            

    def children_variety(self,beta,kind):
        """Return the sibling variety for the children (does not save it)"""
        sibcount = self.child_count(kind)
        c_average = ConceptNode.average_variety(sibcount, beta)
        return c_average

    def average_variety(sibcount, beta):
        """Return the average variety for a group of sibcount siblings"""
        if sibcount == 0:
            c_average = 0
        else:
            c_average =  1 + ((beta ** sibcount - 1) /
                    (sibcount * (beta - 1)))
        return c_average
        
    

    def weighting(self):
        """Return the weighting value for sibling variety"""
        return(ConceptNode.level_weighting(self.level))

 # Warning -- ugly hack.  Need to find a better way to do it
    def level_weighting(level):
        """Return the weighting value for level"""
        if level == 4:
            return(0) # Objectives have zero weighting
        else:
            return (2 ** (level - 1))
        
    def set_parent(self, parent):
        """Assign parent to be the parent of the current node"""
        if self.parent != None:  # we have to eliminate self from the current parent
            self.parent.drop_child(self)
        self.parent = parent
        parent.add_child(self)
        #if parent.child_count() > 1:
        #    self.level = parent.level - 1
        #else:
        #    parent.level = self.level + 1
        #self.dirty = Truetree
        #parent.dirty = True
        #parent.clean_tree(beta)

    def calc_variety(self, beta):
        """Calculate the variety of the whole tree"""
        # TODO -- fix this code or eliminate it
        # NOT YET RIGHT -- DESCENDANT VARIETY IS GOOD< BUT WE DON"T BUILD UP BRANCH RIGHT, I THINK
        self.descendant_variety(beta) # sets the children's level
        self.sibling_variety(beta)
        self.branch_variety(beta)
        self.parent_variety(beta)
        self.allocated_variety(beta)
        #self.dirty = False

    def add_child(self, ch):
        """Add ch as a child"""
        self.children[ch.id] = ch
        #self.dirty = True

    def drop_child(self, ch):
        """Remove ch as a child"""
        del self.children[ch.id]
 #       self.dirty = True

# Not currently being used, I think
    def child_level(self):
        """Set and return the tree level of the node based on the levels of the children"""
        # Both set and return the level
        if self.children == {}:
            self.level = 1
        else:
            self.level = 1 + max(c.child_level() for c in self.children.values())
        if self.fixed_level != 0:
            self.level = self.fixed_level 
        return self.level

#Not currently being used, I think
    def parent_level(self):
        """Set and return the tree level of the node based on the parent, and
            follow changes down the tree if needed """
        if self.parent != None:
            if self.level != self.parent.level - 1 :
                self.level = self.parent.level - 1
                if self.children != {}:
                    for c in self.children.values():
                        c.parent_level()
        if self.fixed_level != 0:
            self.level = self.fixed_level
        return self.level

# Used for dot file group
    def calc_row_pos(self, first_row):
        """Calculate the row position for a node based on the row positions for the children"""
        # This is a recursive call
        temp_row = first_row
        min_row = 99999 #  big number, assumed to be the maximum row count
        max_row = 0
        if self.children == {}:
            self.row = first_row
            min_row = self.row
            max_row = self.row
            last_row = first_row
        else:
            for c in self.children.values():
                last_row = c.calc_row_pos(temp_row)
                if c.row < min_row:
                    min_row = c.row
                if c.row > max_row:
                    max_row = c.row
                temp_row = last_row + 1
            self.row = (min_row + max_row)//2
            self.child_span = [min_row, max_row]
        return(last_row)

    def display_subtree(self, max_level, kind):
        """Display a subtree starting at the given node in a tree having max_level"""
        self.display_node(max_level, kind)
        for ch in self.children.values():
            ch.display_subtree(max_level, kind)

    def display(self):
        """Display a node.  This function is not yet complete"""
        format_str = 'ID: {} row: {}  child_span: {}'
        print (format_str.format(self.id, self.row, self.child_span))

#TODO: Make the tree printout work properly with multiple top-level nodes (recognize nodes with no parent)
    def display_node(self, max_level, kind):
        """Display node information in the context of a tree having max_level"""
        format_str = '  '
        for i in range(1,max_level-self.level):
            if self.parent == None:
                format_str += '  '
            else:
                format_str += '| '
        if self.parent == None:
           format_str += '  '
        else:
            format_str += '|-'
        format_str += 'Label:{}, {} SN:{: .2f}, CN:{: .2f},'
        format_str += 'VB:{: .2f}, '
        format_str += 'CA:{: .2f}, VD:{: .2f}, l:{}, w:{}'
        print (format_str.format(self.node_label, self.id, self.sb_novelty[kind], self.c_novelty[kind], 
                                self.b_variety[kind], 
                                self.c_average[kind], self.d_variety[kind], self.level, self.weighting()))

# deprecated -- no longer used, I think
    def design_space_representation (self, kind = "DSI", s_min = 0, level_offset = -0.5, base_level = 20):
        """Create a TikZ design space representation of the node and and call recursively for the children
kind can be DSI (design space increment), NDSI (novel design space increment), or Novelty (set novelty).
Returns an array containing the maximum x coordinate of the design space and the TikZ string for the design space.
"""
        dsr = ""
        if kind == "DSI":
            space = self.b_variety
        elif kind == "NDSI" :
            space = self.b_increment #Use increment, instead of average 
        elif kind == "Novelty":
            space = self.sb_novelty
        else:
            print ("Invalid value for kind: '{}'; should be 'DSI', 'NDSI', or 'Novelty'".format (kind))
            return ([s_min,''])
        dsr = "\\draw [fill = {}] ({}, {: .2f}) rectangle ( {}, {: .2f}); \n".format(self.fillcolor, level_offset * self.level, s_min, base_level, s_min + space)
        dsr += "\\node [anchor = north west] at  ({: .2f},{: .2f}) {{ {} }};\n".format(level_offset * self.level , s_min + space , self.id)
        c_smin = s_min
        for c in self.children.values():
            cdsr = c.design_space_representation(kind, c_smin, level_offset, base_level)
            c_smin = cdsr[0]
            dsr += cdsr[1]
        return [s_min + space, dsr]
        
        

    def dot_node(self, twidth, plain=False, kind = 'All'):
        """ Create a node in dot tree format """
        #Background colors as a function of level
        colordict = {
            1: "lightpink1",
            2: "palegreen1",
            3: "lightgoldenrod1",
            4: "lightskyblue1"
            }
        mycolor = colordict.get(self.level, "white")
        orig_id = self.id #for debugging purposes
        myTitle = ConceptNode.dot_cleaned_string(self.id)
        wrapped_title = ConceptNode.dot_wrapped_string(myTitle, twidth)
        #print (self.id + ' ' + wrapped_title + ' ' + str(twidth) + str(len(self.id)))
        h_string = '"{}" [shape=none, margin = 0, group = "group{}", label=<\n' \
                   '<TABLE BORDER = "0" CELLBORDER = "1" bgcolor = "{}" cellspacing = "0">\n'
        t_string = '<TR><TD COLSPAN="21"><FONT POINT-SIZE="20">{}</FONT></TD></TR>\n'
        f_string = '</TABLE>>]\n'
        outstring = h_string.format(myTitle, self.row, mycolor)
        outstring += t_string.format(myTitle)
        
        if not plain:
            #print ("Debug: dot_node")
            #print ("Technology")
            #self.display_node(2,'Technology')
            #print ('Tactics')
            #self.display_node(2,'Tactics')
            #print ('Total')
            #self.display_node(2,'Total')
            b_string = ConceptNode.dot_node_string_basic(\
                self.weighting(), self.has_content['Technology'], self.has_content['Tactics'], self.feasibility, \
                self.effectiveness, self.quality, \
                self.c_novelty['Total'], self.c_novelty['Technology'], self.c_novelty['Tactics'])
            outstring += b_string
            if kind == 'All':
                for i in Novelty_Types:
                    n_string = ConceptNode.dot_node_string_novelty(
                        i, self.c_increment[i], self.se_novelty[i], self.sd_novelty[i], \
                        self.c_average[i], self.e_variety[i], self.d_variety[i])
                    outstring += n_string
            else:
                n_string = ConceptNode.dot_node_string_novelty(
                    kind, self.c_increment[kind], self.se_novelty[kind], self.sd_novelty[kind], \
                    self.c_average[kind], self.e_variety[kind], self.d_variety[kind])
                outstring += n_string
        outstring += f_string              
        return(outstring)

    def dot_cleaned_string(in_str):
        """ Return a cleaned string (with special characters replaced by harmless ones) """
        # Substitutions:
        # & -> and
        # + -> and
        # " -> '
        # -> -> to
        # > to over
        
        out_str = in_str
        pat1 = re.compile('&')
        pat2 = re.compile('\+')
        pat3 = re.compile ('"')
        pat4 = re.compile ('->')
        pat5 = re.compile ('>')
      
        p1_str = pat1.sub('and',out_str)
        p2_str = pat2.sub('and',p1_str)
        p3_str = pat3.sub("'", p2_str)
        p4_str = pat4.sub('to', p3_str)
        out_str = pat5.sub('over', p4_str)
    
        return(out_str)
    
    def dot_wrapped_string(in_str, numch):
        """ Return a wrapped string with a maximum width of numch """
        last_wrap = 0
        last_space = 0
        out_str = ""
        if in_str.find(' ', last_wrap + last_space) <= 0:
            #one-word id
            return(in_str)
        while in_str.find(' ', last_wrap + last_space) > 0:
            while (((j := in_str.find(' ',last_wrap + last_space+1)) > 0) and (j - last_wrap <= numch)):
                last_space = j-last_wrap
            out_str += in_str[last_wrap:last_wrap + last_space]
            if j > 0 or len(in_str) - last_wrap > numch:
                out_str += '<br/>'
            last_wrap = last_wrap + last_space + 1
            last_space = 0
            if j<0:
                out_str += ' ' + in_str[last_wrap:len(in_str)]
                last_wrap = len(in_str)
                last_space = 0
        return(out_str)

    def dot_node_string_basic(weight, has_tech, has_tac, feasibility, effectiveness, quality, \
                        total_novelty, tech_novelty, tac_novelty):
        """ Create a formatted node string for the basic characteristics of a node """
        f_string = '<TR><TD COLSPAN="3"> Idea </TD> ' \
                   '<TD COLSPAN="2"> W:{:.2f} </TD> '\
                   '<TD COLSPAN="2"> Te:{} </TD> '\
                   '<TD COLSPAN="2"> Ta:{} </TD> '\
                   '<TD COLSPAN="2"> F:{:.2f} </TD> '\
                   '<TD COLSPAN="2"> E:{:.2f} </TD> '\
                   '<TD COLSPAN="2"> Q:{:.2f} </TD> '\
                   '<TD COLSPAN="2"> N<sub>Te</sub>:{:.2f} </TD> '\
                   '<TD COLSPAN="2"> N<sub>Ta</sub>:{:.2f} </TD> '\
                   '<TD COLSPAN="2"> N:{:.2f} </TD> </TR>\n '
        return (f_string.format(weight, has_tech, has_tac, feasibility, \
                               effectiveness, quality, tech_novelty, tac_novelty, total_novelty))

    def dot_node_string_novelty(kind, c_i, s_e, s_d, c_a, v_e, v_d):
        """ Create a formatted node string for a novelty/variety row for a node """
        f_string = '<TR><TD COLSPAN="3"> {} </TD> '\
                   '<TD COLSPAN="3"> C<sub>I</sub>:{:.2f} </TD> '\
                   '<TD COLSPAN="3"> S<sub>E</sub>:{:.2f} </TD> '\
                   '<TD COLSPAN="3"> S<sub>D</sub>:{:.2f} </TD> '\
                   '<TD COLSPAN="3"> C<sub>A</sub>:{:.2f} </TD> '\
                   '<TD COLSPAN="3"> V<sub>E</sub>:{:.2f} </TD> '\
                   '<TD COLSPAN="3"> V<sub>D</sub>:{:.2f} </TD> </TR>\n'
        return (f_string.format(kind, c_i, s_e, s_d, c_a, v_e, v_d))


                            

    def dot_children(self):
        """ Write the links to the children, in dot tree format """
        tree_string = ""
        for  c in self.children.values():
            tree_string += '"{}" -- "{}" [headport = "w"]\n'.format(ConceptNode.dot_cleaned_string(self.id), \
                                                                    ConceptNode.dot_cleaned_string(c.id))
        return(tree_string)

#TODO -- decide if this needs kind added
    def dot_group_link(self, tree):
        """Write an invisible link to a group member"""
        row = self.row
        if self.children == {}:
            return ('not needed') # no children, so no invisible link needed
        for c in self.children.values():
            if c.row == row:
                return ('not needed') # row is part of children, so no invisible link needed
        for n in tree.nodes.values():
            if n != self and n.parent != self and n.row == self.row:
                return ('"{}" -- "{}" [style=invis]\n'.format(ConceptNode.dot_cleaned_string(self.id), ConceptNode.dot_cleaned_string(n.id)))
        return ('not found')
        
        
        




# Testing Data

#TODO: Eliminate fill and positioning

treeTwo = ConceptTree('TreeTwo')
Ofill = "blue!20"
Pfill = "yellow!50"
Efill = "violet!20"
Dfill = "red!20"
O1 = treeTwo.add_node(ConceptNode('Objective 1','O1', fixed_level = 4, fill = Ofill, positioning = "left = 6mm of P1, yshift = 3cm"))
P1 = treeTwo.add_node(ConceptNode('Principle 1', 'P1', O1, fill=Pfill,positioning = "left = of E1"))
P2 = treeTwo.add_node(ConceptNode('Principle 2', 'P2', O1, fill = Pfill, positioning = "left = of E3"))
E1 = treeTwo.add_node(ConceptNode('Embodiment 1', 'E1', P1, fill = Efill, positioning = "left = of D2"))
E2 = treeTwo.add_node(ConceptNode('Embodiment 2', 'E2', P1, fill = Efill, positioning = "left = of D4"))
E3 = treeTwo.add_node(ConceptNode('Embodiment 3', 'E3', P2, fill = Efill, positioning = "left = of D6"))
D1 = treeTwo.add_node(ConceptNode('Detail 1', 'D1', E1, fill = Dfill))
D2 = treeTwo.add_node(ConceptNode('Detail 2', 'D2', E1, fill = Dfill, positioning = "above = of D1"))
D3 = treeTwo.add_node(ConceptNode('Detail 3', 'D3', E1, fill = Dfill, positioning = "above = of D2"))
D4 = treeTwo.add_node(ConceptNode('Detail 4', 'D4', E2, fill = Dfill, positioning = "above = of D3"))
D5 = treeTwo.add_node(ConceptNode('Detail 5', 'D5', E2, fill = Dfill, positioning = "above = of D4"))
D6 = treeTwo.add_node(ConceptNode('Detail 6', 'D6', E3, fill = Dfill, positioning = "above = of D5"))



D1.set_concept_novelty ('Technology', 0.5)
D1.set_concept_novelty ('Tactics', 0.5)
D2.set_concept_novelty ('Total',  1)
D3.set_concept_novelty ('Technology', 0.5)
D4.set_concept_novelty ('Tactics', 0.5)
D5.set_concept_novelty ('Technology', 0.707)
D5.set_concept_novelty ('Tactics', 0.707)
D6.set_concept_novelty ('Technology', 0.7)

treeTwo.set_content()
treeTwo.calc_variety('All')
#print ('After tree calc_variety All')
treeTwo.calc_novelty('All')
#print ('After tree calc_novelty All')
treeTwo.calc_row_positions()
O1 = treeTwo.nodes['Objective 1']
O1.dot_group_link(treeTwo)
#print ('treeTwo Variety (total): {:.2f}, Novelty (total): {:.2f}\n'.format(treeTwo.variety['Total'], treeTwo.novelty['Total']))
#print ('treeTwo Variety (tactics): {:.2f}, Novelty (tactics): {:.2f}\n'.format(treeTwo.variety['Tactics'], treeTwo.novelty['Tactics']))
#print ('treeTwo Variety (technology): {:.2f}, Novelty (technology): {:.2f}\n'.format(treeTwo.variety['Technology'], treeTwo.novelty['Technology']))

treeTwo.save_file('TwoJson.txt')
treeTwo.save_dot_tree()
treeTwo.save_dot_tree(kind = 'Total')
treeTwo.save_dot_tree(kind = 'Technology')
treeTwo.save_dot_tree(kind = 'Tactics')
treeTwo.save_dot_tree_plain()


#E3.calc_novelty('Technology')
#treeTwo.save_dot_tree()
