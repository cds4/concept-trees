import csv
import sys
from ConceptTree import ConceptTree, ConceptNode

print (sys.argv)


TreeName = 'D1top' # File name for exported latex files
ImportFile = 'D1top.csv' # file name for csv to be imported
ObjectiveName = 'Prevent Home Invasion Damage' # Design Objective (not in tree)
KeyPositioning = "above = 5cm of O1" #TikZ positioning command for Variety/Novelty key

def read_ideas(f_name):
    entry_list = []
    infile = open(f_name, 'r')
    csv_reader = csv.DictReader(infile)
    for row in csv_reader:
        entry_list.append(dict(row))
    infile.close()
    return(entry_list)

def get_principle_code(p_text):
    """Find the principle code for a principle having the text p_text.
If the principle is found, just return the code.
If the principle is not found, add a new principle to the principles list,
and return the code """
    global principle_count
    if p_text == "":
        return ("") # No principle to look for
    for p in principles.items(): 
        if p[1]['Name'] == p_text:
            return (p[0])
    # not found, so add
    principle_count += 1
    p_code = 'P' + str(principle_count)
    principles[p_code] = {'Name':p_text, 'Parent': "", 'Novelty':''}
    #print(p_code)
    return(p_code)
    

def get_embodiment_code(e_text, p_text):
    """Find the embodiment code for an embodiment having the text e_text.
If the embodiment is found, just return the code.
If the embodiment is not found, add a new embodiment to the embodiments list,
get the principle code to add to the embodiment (if the p_text is not empty),
and return the embodiment code """
    global embodiment_count
    #print ('In get_embodiment_code\n  e_text ' + e_text + '\n  p_text ' + p_text)
    if e_text == "":
        return("") # no embodiment to look for
    for e in embodiments.items():
        #print ('e ' + e[0] + ': Name ' + e[1]['Name'])
        if e[1]['Name'] == e_text:
            return (e[0])
    # not found, so add
    embodiment_count += 1
    e_code = 'E' + str(embodiment_count)
    p_code = get_principle_code(p_text)
    embodiments[e_code] = {'Name':e_text,'Parent':p_code, 'Novelty':''}
    #print(e_code)
    return(e_code)

def get_detail_code (d_text, e_text, p_text):
    """Find the detail code for a detail having the text d_text.
If the detail is found, print a warning and don't add a new one.
If the detail is not found, add a new detail to the details list,
get the embodiment code to add to the detail (if the e_text is not empty),
and return the detail code """
    global detail_count
    #print ('in get_detail_code \n' + d_text + ' \n' + e_text + '\n ' + p_text)
    if d_text == "":
        return ("")
    for d in details.items():
        #print (d)
        if d[1]['Name'] == d_text:
            return (d[0])
    # not found, so add
    detail_count += 1
    d_code = 'D' + str(detail_count)
    e_code = get_embodiment_code(e_text, p_text)
    details[d_code] = {'Name':d_text,'Parent':e_code, 'Novelty':''}
    #print(d_code)
    return(d_code)
    
arg_count = len(sys.argv)-1

if arg_count == 0:
    print ("No arguments found.  Using defaults in import-csv.py")
if arg_count == 1:
    ImportFile = sys.argv[1]
    dot_pos = ImportFile.index('.',0)
    TreeName = ImportFile[:dot_pos]
    # Use defaults for rest
    #ObjectiveName = TreeName
    #KeyPositioning = "above = 5cm of O1" #TikZ positioning command for Variety/Novelty key
if arg_count == 2:
    ImportFile = sys.argv[1]
    TreeName = sys.argv[2]
    #Use defaults for rest
    #ObjectiveName = TreeName
    #KeyPositioning = "above = 5cm of O1" #TikZ positioning command for Variety/Novelty key
if arg_count == 3:
    ImportFile = sys.argv[1]
    TreeName = sys.argv[2]
    ObjectiveName = sys.argv[3]
    #Use Defaults for rest
    #KeyPositioning = "above = 5cm of O1" #TikZ positioning command for Variety/Novelty key
if arg_count == 4:
    ImportFile = sys.argv[1]
    TreeName = sys.argv[2]
    ObjectiveName = sys.argv[3]
    KeyPositioning = argv[4]
    
ideas = {}
principles = {}
embodiments = {}
details = {}
principle_count = 0
embodiment_count = 0
detail_count = 0
    
    

imported_ideas = read_ideas(ImportFile)

Ofill = "blue!20"
Pfill = "yellow!50"
Efill = "green!20"
Dfill = "red!20"

current_principle = ""
current_embodiment = ""
current_detail = ""

for id in imported_ideas:
    idea = id['Idea']
    p_no = id['Principle_No']
    if id['Principle_No'] != '0':
        if id['Detail']!= "":
            d_code = get_detail_code(id['Detail'],id['Embodiment'],id['Principle'])
            details[d_code]['Novelty'] = id['Novelty']
        elif id['Embodiment']!= "":
            e_code = get_embodiment_code(id['Embodiment'],id['Principle'])
            embodiments[e_code]['Novelty'] = id['Novelty']
        elif id['Principle'] != "":
            p_code = get_principle_code(id['Principle'])
            principles[p_code]['Novelty'] = id['Novelty']
        if id['Code'] != "":
            ideas[id['Code']] = {'Level':id['Level'], 'Idea':id['Idea']}
       
treeVar = 'tree'+TreeName
vars()[treeVar]=ConceptTree(TreeName)

o_ref = int(len(principles)/2)
o_ref_node = 'P'+ str(o_ref)

O1 = vars()[treeVar].add_node(ConceptNode(ObjectiveName,'O1',fixed_level=4, fill=Ofill, \
                                          positioning = 'left = of ' + o_ref_node + ','))                      

for p in principles.items():
    id = p[0]
    name = p[1]['Name']
    parent = p[1]['Parent']
    vars()[id] = vars()[treeVar].add_node(ConceptNode(name,id,fixed_level=3, fill=Pfill))
    if p[1]['Novelty'] != "":
        vars()[id].c_novelty = float(p[1]['Novelty'])
    if parent != "":
        vars()[id].set_parent(vars()[parent])
    else:
        vars()[id].set_parent(O1)
    #vars()[treeVar].display_tree()


for e in embodiments.items():
    id = e[0]
    name = e[1]['Name']
    parent = e[1]['Parent']
    vars()[id] = vars()[treeVar].add_node(ConceptNode(name,id,fixed_level=2, fill=Efill))
    if e[1]['Novelty'] != "":
        vars()[id].c_novelty = float(e[1]['Novelty'])
    if parent != "":
        vars()[id].set_parent(vars()[parent])

for d in details.items():
    id = d[0]
    name = d[1]['Name']
    parent = d[1]['Parent']
    vars()[id] = vars()[treeVar].add_node(ConceptNode(name,id,fixed_level=1, fill=Dfill))
    if d[1]['Novelty'] != "":
        vars()[id].c_novelty = float(d[1]['Novelty'])
    if parent != "":
        vars()[id].set_parent(vars()[parent])

p_count = len(principles)
e_count = len(embodiments)
d_count = len(details)

max_count = max(p_count, e_count, d_count)

ordered_dict={}
index = 0
if max_count == p_count: # more principles than anything
    for p in range(1,p_count+1):
        id = 'P'  + str(p)
        ref_id = 'P' + str(p-1)
        if p > 1:
            vars()[id].positioning = 'below = of ' + ref_id + ','
        ordered_dict[index] = vars()[id]
        index += 1
    for e in range(1, e_count+1):
        curr_id = 'E' + str(e)
        prev_id = 'E' + str(e-1)
        if e == 1:
            vars()[curr_id].positioning = 'right = of P1'
        else:
            if vars()[curr_id].parent != None:
                parent_id=vars()[curr_id].parent.node_label
                prev_parent_id = vars()[prev_id].parent.node_label
                parent_id_num = int(parent_id[1:])
                if prev_parent_id != parent_id:
                    vars()[id].positioning = 'right = of P' + str(e) + ','
                else:
                    vars()[id].positioning = 'below = of ' + prev_id +','
        ordered_dict[index] = vars()[id]
        index += 1
        
    for d in range(1,d_count+1):
        curr_id = 'D' + str(d)
        prev_id = 'D' + str(d-1)
        if d == 1:
           vars()[curr_id].positioning = 'right = of E1,'
        else:
            if vars()[curr_id].parent != None:
                parent_id=vars()[curr_id].parent.node_label
                prev_parent_id = vars()[prev_id].parent.node_label
                parent_id_num = int(parent_id[1:])
                if prev_parent_id != parent_id:
                    vars()[curr_id].positioning = 'right = of E' + str(d) + ','
                else:
                    vars()[curr_id].positioning = 'below = of ' + prev_id + ','
        ordered_dict[index] = vars()[id]
        index += 1
                   
elif max_count == e_count: # more embodiments than anything
    for e in range( 1,e_count+1):
        id = 'E'  + str(e)
        prev_id = 'E' + str(e-1)
        # position each embodiment below the previous one
        if e > 1:
            vars()[id].positioning = 'below = of ' + prev_id +','
        ordered_dict[index] = vars()[id]
        index += 1
        # position each principle to the left of one of its children
        if vars()[id].parent != None :
            if vars()[id].parent.positioning == "" :
                vars()[id].parent.positioning = 'left = of ' + id +','
    # add the parents to the ordered array
    for p in range(1, p_count+1):
        id = 'P' + str(p)
        ordered_dict[index] = vars()[id]
        index += 1
    for d in range(1, d_count+1):
        id = 'D' + str(d)
        # position each detail to either the right of its parent or below its siblings
        curr_id = 'D' + str(d)
        prev_id = 'D' + str(d-1)
        if d == 1:
            vars()[curr_id].positioning = 'right = of E1,'
        else:
            if vars()[curr_id].parent != None:
                parent_id=vars()[curr_id].parent.node_label
                prev_parent_id = vars()[prev_id].parent.node_label
                parent_id_num = int(parent_id[1:])
                if prev_parent_id != parent_id:
                    #if d >= int(parent_id[1:]): #check was insufficient -- need to know if family, not just detail
                        vars()[curr_id].positioning = 'right = of E' + str(d) + ','
                    #else:
                    #    vars()[curr_id].positioning = 'right = of ' + parent_id + ','
                else:
                    vars()[curr_id].positioning = 'below = of ' + prev_id + ','
        ordered_dict[index] = vars()[id]
        index += 1


elif max_count == d_count: #more details than anything
    for d in range( 1, d_count+1):
        id = 'D'  + str(d)
        ref_id = 'D' + str(d-1)
        if d > 1:
            vars()[id].positioning = 'below = of ' + ref_id + ','
        if vars()[id].parent != None:
            if vars()[id].parent.positioning == "" :
                vars()[id].parent.positioning = 'left = of ' + id + ','
        ordered_dict[index] = vars()[id]
        index += 1
    for e in range( 1 , e_count+1):
        id = 'E' + str(e)
        if vars()[id].parent != None:
            if vars()[id].parent.positioning == "" :
                vars()[id].parent.positioning = 'left = of ' + id + ','
        ordered_dict[index] = vars()[id]
        index += 1
    for p in range(1, p_count+1):
        id = 'P' + str(p)
        if vars()[id].parent != None:
            if vars()[id].parent.positioning == "" :
                vars()[id].parent.positioning = 'left = of ' + id + ','
        ordered_dict[index] = vars()[id]
        index += 1
        
ordered_dict[index] = O1

v = vars()[treeVar].calc_variety()
min_v = vars()[treeVar].minimum_variety()
max_v = vars()[treeVar].maximum_variety()
norm_v = (v - min_v)/(max_v - min_v)

print ('Total Variety: {:.2f}\n'  \
       'Minimum Variety: {:.2f}\n'  \
       'Maximum Variety: {:.2f}\n'  \
       'Normalized Variety: {:.2f}\n'  \
       'Total Set Novelty {:.2f}\n'  \
       'Quantity {}\n'.format(\
    v,\
    min_v,\
    max_v,\
    norm_v,\
    vars()[treeVar].calc_novelty(),\
    vars()[treeVar].calc_quantity()))

vars()[treeVar].save_latex_standalone(ordered_dict,KeyPositioning)           
vars()[treeVar].save_latex_plain_standalone(ordered_dict)           
vars()[treeVar].calc_row_positions()         
vars()[treeVar].save_dot_tree()         
vars()[treeVar].save_dot_tree_plain()

       
