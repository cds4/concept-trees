
from math import sqrt
import json
import copy
import re

# For python 3.10

Version = "1.3"

class ConceptTree():
    """A tree of Concept Nodes, with multiple Levels"""
    def __init__(self, id, beta = 0.7):
        self.id = id
        self.max_level = 0
        self.nodes = {}
        self.beta = beta
        self.version = Version

    def add_node(self,node):
        """Add node to tree"""
        self.nodes[node.id] = node
        self.calc_levels()
        self.calc_novelty() # variety calculation is at start of novelty calc
        return node
        
    def remove_node(self,node):
        """Remove node from tree"""
        for n in self.nodes.values():
            if n != node:   #check for relationships
                if node in n.children.values(): # found a child relationship
                    del n.children[node.id]
                if n.parent == node: #found a parent relationship
                    n.parent = None
        del self.nodes[node.id]
        self.calc_levels()
        self.calc_novelty()
        
    def list_nodes(self):
        """List the nodes in the tree"""
        list(self.nodes)

    def save_file(self, filename):
        f=open(filename,'w')
        f.write(json.dumps(self.export_json()))
        f.close

    def load_file(self, filename):
        """ Create a new concept tree from the data stored in a json file"""
        f=open(filename, 'r')
        j = f.readlines()[0]
        return self.import_json(j)

    def save_latex(self, order_dict = {}):
        """ Save LaTeX node descriptions for the concept tree """
        output_file = self.id + "SetProperties.tex"
        of = open(output_file, 'w')
        self.export_latex(of, order_dict)
        of.close

    def save_latex_standalone(self, order_dict = {},key_positioning = ''):
        """ Save LaTeX node descriptions for the concept tree as standalone latex"""
        output_file = self.id + ".tex"
        of = open(output_file, 'w')
        self.export_latex_standalone(of, order_dict, key_positioning)
        of.close
            
    def save_latex_plain_standalone(self, order_dict={}):
        """ Save LaTeX node descriptions for the concept tree as standalone latex without calculations"""
        output_file = self.id + "plain.tex"
        of = open(output_file, 'w')
        self.export_latex_plain_standalone(of,order_dict)
        of.close

    def save_latex_novelty_standalone(self):
        """ Save LaTeX diagram of the Novelty as standalone image"""
        output_file = self.id + "Novelty.tex"
        of = open(output_file, 'w')
        self.export_space_rep(of,'Novelty')
        of.close

    def save_latex_novel_design_space_standalone(self):
        """ Save LaTeX diagram of the novel design space as standalone image"""
        output_file = self.id + "NovelDesignSpace.tex"
        of = open(output_file, 'w')
        self.export_space_rep(of,'NDSI')
        of.close

    def save_latex_design_space_standalone(self):
        """ Save LaTeX diagram of the design space as standalone image"""
        output_file = self.id + "DesignSpace.tex"
        of = open(output_file, 'w')
        self.export_space_rep(of,'DSI')
        of.close

    def export_json(self):
        """Export the nodes in the tree in JSON format"""
        n_dict = {}
        for n in self.nodes.keys():
            sn = self.nodes[n]
            if sn.children == {}:
                novelty = self.nodes[n].c_novelty
            else:
                novelty = 0
            p = sn.parent
            if p != None:
                p = p.id
            n_dict[n]= { 'parent':p, 'label':sn.node_label,'positioning':sn.positioning, 'novelty':novelty,
                         'color':sn.fillcolor, 'fixed_level':sn.fixed_level}
        return { 'id':self.id, 'max_level':self.max_level, 'beta':self.beta, 'nodes': n_dict }
            
    def import_json(self, str):
        """Import a tree from a JSON string"""
        j = json.loads(str)
        t = ConceptTree(j['id'])
        # first, add all the nodes
        for nid in j['nodes'].keys():
            t.add_node(ConceptNode(nid))
        # now, add other values
        for nid in j['nodes'].keys():
            jn = j['nodes'][nid]
            tn = t.nodes[nid]
            # add parent
            if jn['parent'] != None:
                pid = jn['parent']
                p = t.nodes[pid]
                tn.set_parent(p)
            # add auxiliary data
            tn.node_label = jn['label']
            tn.positioning = jn['positioning']
            tn.c_novelty = jn['novelty']
            tn.fillcolor = jn['color']
            tn.fixed_level = jn['fixed_level']
        # calculate the levels,                             
        t.calc_levels()
        t.beta = j['beta']
        t.calc_novelty()
        return t
        
        

    def display_tree(self):
        """Make a text display of a tree, with children indented"""
        t_nodes = self.top_nodes()
        for n in t_nodes:
            n.display_subtree(self.max_level)

    def sorted_nodes(self):
        """Create a dict of the nodes of the tree, sorted by
node positioning commands."""
        node_dict = {} # a dictionary of nodes {index:node}
        ref_dict = {} # a dictionary of positioning references {index:node_dict index}
        s_nodes = {} # a dictionary of sorted nodes {sort_index:node}
        #fill up node_array and ref_array
        index = 0
        for n in self.nodes.values():
            node_dict[index] = n
            ref_dict[index] = ''
            s_nodes[index]= ''
            index += 1
        # where node label k is found in positioning for node j,
        # add node label k in ref_array[j]
        for j in range(len(node_dict)):
            pos = node_dict[j].positioning +','
            for k in range(len(node_dict)):
                if node_dict[k].node_label+',' in pos:
                    ref_dict[j] = k
                    break
        #s_nodes = {} # the sorted nodes, in a sort_index:node dictionary
        #search for all nodes that have no positioning references and add
        # them to the top of s_nodes (the nodes) and node_orders
        index = 0
        for i in range(len(node_dict)):
            if ref_dict[i]=='':
                s_nodes[index]=node_dict[i]
                index += 1
        for i in range(len(s_nodes)):
            referenced_node_label = s_nodes[i].node_label
            for j in range(len(node_dict)):
                if (ref_dict[j] != '') and (node_dict[ref_dict[j]].node_label == referenced_node_label):
                    s_nodes[index] = node_dict[j]
                    index += 1
        return(s_nodes)

    def export_latex_standalone (self, f_latex, order_dict, key_positioning):
        """Export the tree as a latex standalone document"""
        # Write the preamble
        print ( "\\documentclass {standalone}\n"\
                "\\usepackage{tikz}\n"\
                "\\usetikzlibrary{concepttree,positioning}\n"\
                "\\begin{document}\n"\
                "\\begin{tikzpicture}[node distance = 7mm, align=center, every text node part/.style={text width = 3cm}]\n"\
                "\\scriptsize",\
                file = f_latex)
        # Export the nodes
        self.export_latex(f_latex, order_dict)
        # Write the legend (NOTE: It needs a reference node called d1, usually the left-most detail)
        print ( "  \\node (key)[shape=conceptnode, draw, {} ]".format(key_positioning), file=f_latex)
        print ( "  { Element%\n"\
                "    \\nodepart{novelty}$N$%\n"\
                "    \\nodepart{contributionincrement}$C_i$%\n"\
                "    \\nodepart{setnoveltyelement}$S_E$%\n"\
                "    \\nodepart{setnoveltydescendant}$S_D$%\n"\
                "    \\nodepart{weight}$w$%\n"\
                "    \\nodepart{contributionaverage}$C_A$%\n"\
                "    \\nodepart{varietyelement}$V_E$%\n"\
                "    \\nodepart{varietydescendant}$V_D$%\n"\
                "  };%\n"\
                "  \\node (keylabel)[above=1 mm of key]{Shape key:};\n"\
                "\\end{tikzpicture}\n"\
                "\\end{document}",\
                file = f_latex)
        
    def export_latex_plain_standalone (self, f_latex, order_dict):
        """Export the tree as a latex standalone document without calculations"""
        # Write the preamble
        print ( "\\documentclass {standalone}\n"\
                "\\usepackage{tikz}\n"\
                "\\usetikzlibrary{positioning}\n"\
                "\\begin{document}\n"\
                "\\begin{tikzpicture}[node distance = 7mm, align=center, every node/.style={text width = 3cm}]\n"\
                "\\scriptsize",\
                file = f_latex)
        # Export the nodes
        self.export_latex_plain(f_latex, order_dict)
        # End the document
        print ( "\\end{tikzpicture}\n"\
                "\\end{document}",\
                file = f_latex)

    def export_latex(self, f_latex, order_dict):
        """Export the tree in format appropriate for displaying in LaTeX"""
        if order_dict == {}:
            order_dict = self.sorted_nodes() # dict of {sort_order:node}
        # first, export the variety nodes
        print ("% BEGINNING OF NODES", file = f_latex)
        for index in range(len(order_dict)):
            n=order_dict[index]
            print ( "\\node ({}) %".format(n.node_label), file = f_latex)
            print ( "  [shape=conceptnode, fill={}, draw, {}]%".format(n.fillcolor, n.positioning), file = f_latex)
            print ( "  {{ {}%".format(n.id), file = f_latex)
            print ( "  \\nodepart{{weight}}{:.2f}%".format(n.weighting()), file = f_latex)
            print ( "  \\nodepart{{contributionaverage}}{:.2f}%".format(n.c_average), file = f_latex)
            print ( "  \\nodepart{{varietyelement}}{:.2f}%".format(n.e_variety), file = f_latex)
            print ( "  \\nodepart{{varietydescendant}}{:.2f}%".format(n.d_variety), file = f_latex)
            print ( "  \\nodepart{{novelty}}{:.2f}%".format(n.c_novelty), file = f_latex)
            print ( "  \\nodepart{{contributionincrement}}{:.2f}%".format(n.c_increment), file = f_latex)
            print ( "  \\nodepart{{setnoveltyelement}}{:.2f}%".format(n.se_novelty), file = f_latex)
            print ( "  \\nodepart{{setnoveltydescendant}}{:.2f}%".format(n.sd_novelty), file = f_latex)
            print ( "};", file = f_latex)
        print ( "% END OF NODES", file = f_latex )
        print ( "\n% draw lines\n", file = f_latex )
        for index in range(len(order_dict)):
            n=order_dict[index]
            if n.children != {} :
                # we have children, so draw the lines
                for c in n.children:
                    cn = self.nodes[c]
                    print ( "\\draw[thick]({}.east) -- ({}.west);".format(n.node_label,cn.node_label), file = f_latex)
        print ( "\n% end of lines\n",file = f_latex)

    def export_latex_plain(self, f_latex, order_dict):
        """Export the tree in format appropriate for displaying in LaTeX without calculations"""
        if order_dict == {}:
            order_dict = self.sorted_nodes() # dict of {sort_order:node}
        # first, export the variety nodes
        print ("% BEGINNING OF NODES", file = f_latex)
        for index in range(len(order_dict)):
            n=order_dict[index]
            print ( "\\node ({}) %".format(n.node_label), file = f_latex)
            print ( "  [shape=rectangle, fill={}, draw, {}]%".format(n.fillcolor, n.positioning), file = f_latex)
            print ( "  {{ {}%".format(n.id), file = f_latex)
            print ( "};", file = f_latex)
        print ( "% END OF NODES", file = f_latex )
        print ( "\n% draw lines\n", file = f_latex )
        for index in range(len(order_dict)):
            n=order_dict[index]
            if n.children != {} :
                # we have children, so draw the lines
                for c in n.children:
                    cn = self.nodes[c]
                    print ( "\\draw[thick]({}.east) -- ({}.west);".format(n.node_label,cn.node_label), file = f_latex)
        print ( "\n% end of lines\n",file = f_latex)
        
    def export_space_rep(self, f_latex, s_kind):
        # Write the preamble
        print ( "\\documentclass {standalone}\n"\
                "\\usepackage{tikz}\n"\
                "\\usetikzlibrary{positioning}\n"\
                "\\begin{document}\n"\
                "\\begin{tikzpicture}\n"\
                "\\scriptsize",\
                file = f_latex)
        # Write the nodes
        print (self.create_ds_representation(s_kind), file = f_latex)
        # End the document
        print ( "\\end{tikzpicture}\n"\
                "\\end{document}",\
                file = f_latex)
        
    def create_ds_representation (self, kind="DSI"):
        """ Create a design space representation for a tree.  kind can be 'DSI', 'NDSI', or 'Novelty'.
Returns a string that kan be used with TikZ to create the representation in a pdf file."""
        xmin = 0
        dsr = ""
        for n in self.top_nodes():
            n_rep = n.design_space_representation (kind, xmin)
            xmin += n_rep[0]
            dsr += n_rep[1]
        return (dsr)
        

       
#TODO: Figure out how to handle parent-child relationships including partial trees
    def calc_levels(self):
        """Calculate the tree levels of all nodes in the tree, and set the maximum level"""
        t_nodes = self.top_nodes()
        # find the level based on the children
        #for n in t_nodes:
            #if len(n.children.values()) == 0:
                #if n.level == 0 :
 #                   n.level = 1 # default value
            #if len(n.children.values()) > 0:
 #               n.level = 1 + max(c.child_level() for c in n.children.values())
            #if n.fixed_level != 0:
 #               n.level = n.fixed_level
        # adjust as needed for parent levels
        #for n in t_nodes:
            #for c in n.children.values():
                #c.parent_level()
        for n in t_nodes:
            n.level = n.fixed_level
            cv = n.children.values()
            if len(cv) > 0:
                for c in cv:
                    c.child_level()
        self.max_level = max(n.level for n in t_nodes)
           
    def top_nodes(self):
        """Return a list containing the nodes in the tree with no parents"""
        t_nodes = []
        for n in self.nodes.values():
            if n.parent == None:
                t_nodes.append(n)
        return t_nodes

    def calc_row_positions(self):
        """Calculate the desired row positions for every node in the tree"""
        for n in self.nodes.values():
            self.row=-1
            self.child_span = [-1, -1]
        first_row = 1
        for t in self.top_nodes():
            last_row = t.calc_row_pos(first_row)
            first_row = last_row + 1

    def calc_quantity(self):
        """Caclulate the quantity of ideas in the tree (the number of leaves)"""
        quantity = 0
        for n in self.nodes.values():
            if n.children == {}:
                quantity += 1
        return(quantity)
                


    def calc_variety(self):
        """Perform variety calculations on the tree"""
        total_variety = 0
        for t in self.top_nodes():
            t.branch_variety(self.beta)
            total_variety += t.b_variety
        for t in self.top_nodes():
            t.allocated_variety()
        return(total_variety)

    def minimum_variety(self):
        """Calculate the minimum variety possible in a tree with the same number
           and kinds of nodes"""
        element_count = [0, 0, 0, 0]
        min_v = 0
        for n in self.nodes.values():
            element_count[n.level-1]+=1
        for i in range(0,4):
            min_v += ConceptNode.level_weighting(i+1) * \
                     ConceptNode.average_variety(element_count[i],self.beta) * \
                     element_count[i]
        return(min_v)    

    def maximum_variety(self):
        """Calculate the maximum variety possible in a tree with the same number
           and kinds of nodes"""
        element_count = [0, 0, 0, 0]
        f_max = [0, 0, 0, 0]
        n_max = [0, 0, 0, 0]
        s_max = [0, 0, 0, 0]
        d_max = [0, 0, 0, 0]
        b_max = [0, 0, 0, 0]
        min_v = 0
        for n in self.nodes.values():
            element_count[n.level-1]+=1
        f_max[3]=element_count[3]
        n_max[3] = 1
        for i in range(3, 0, -1):
            k = i-1
            f_max[k] = element_count[k+1]
            n_max[k] = element_count[k]/element_count[k+1]
            s_max[k] = ConceptNode.average_variety(n_max[k],self.beta)
        b_max[0] = ConceptNode.level_weighting(1)*s_max[0]
        for k in range(1,4):
            d_max[k] = n_max[k-1]*b_max[k-1]  
            b_max[k] = ConceptNode.level_weighting(k+1)*s_max[k]+d_max[k]
        return b_max[3]
        

    def calc_novelty(self):
        """Perform novelty calculations on the tree"""
        self.calc_variety()
        total_novelty = 0
        for t in self.top_nodes():
            t.calc_novelty() #first get the concept novelty
            t.calc_set_novelty(self.beta) # This is a recursive function over the whole branch
            total_novelty += t.sb_novelty
        return(total_novelty)

    def calc_quality(self):
        """Calculate the set quality based on the idea quality"""
        set_quality = 0
        q_thresh = 2 # minimum quality level to count for a quality idea
        for n in self.nodes.values():
            if n.quality >= q_thresh:
                set_quality += 1
        return(set_quality)
        
    def save_dot_tree(self, twidth = 20, sdist = 0.5, rankdist = 1.5): 
        """ Save graphviz (dot) node descriptions to create a graphical representation of a tree"""
        output_file = self.id + ".dot"
        of = open(output_file, 'w')
        self.export_dot_tree(of, twidth, sdist, rankdist)
        of.close

    def save_dot_tree_plain(self, twidth = 20, sdist = 0.5, rankdist = 1.5): 
        """ Save graphviz (dot) node descriptions to create a graphical representation of a tree"""
        output_file = self.id + "Plain.dot"
        of = open(output_file, 'w')
        self.export_dot_tree(of, twidth, sdist, rankdist, True)
        of.close


    def export_dot_tree (self, f_dot, twidth, sdist, rankdist, plain = False):
        """Export the OPED Tree in dot file import mode"""
        # Write the preamble
        preamble = "strict graph {{\n" \
                   'rankdir = "LR"\n' \
                   'splines = true\n' \
                   'ranksep = {}\n' \
                   'nodesep = {}'.format(rankdist, sdist)
        print ( preamble, file = f_dot)
        # Export the nodes
        self.export_dot_nodes (f_dot, twidth, plain)
        # Write the conclusion
        print ( "}",file = f_dot)

    def export_dot_nodes(self, f_dot, twidth, plain = False):
        """Export the OPED Tree nodes"""
        node_dict = self.nodes
        #First write the nodes
        for n in node_dict.values():
            n_node = n.dot_node(twidth, plain)
            f_dot.write("{}\n".format(n_node))
        
        #Write child relationships for each course in level order
        for l in range(self.max_level, 0, -1):
            for n in node_dict.values():
                if n.level == l:
                    child_string = n.dot_children()
                    f_dot.write(child_string)

        #Write in the non-child group links
        for n in node_dict.values():
            group_link = n.dot_group_link(self)
            if group_link != 'not needed' and group_link != 'not found':
                f_dot.write(group_link)

        # Write in the key
        if plain == False:
            Key = ConceptNode('Key')
            Key.row = 1
            KeyChild = ConceptNode('Key Child') # Dummy node to make alignment work
            Key.children[KeyChild.id]=KeyChild
            wrapped_title = ConceptNode.dot_wrapped_string('Element Name', twidth)
            #print (wrapped_title + ' ' + str(twidth) + ' ' + str(len(wrapped_title)))
            f_string = \
                        '"Key" [shape=none, margin = 0, group = "group1", label=<\n' \
                        '<TABLE BORDER = "0" CELLBORDER = "1" bgcolor = "white" cellspacing = "0">\n' \
                        '<TR><TD colspan="4" border="0">Shape Key:</TD></TR>\n' \
                        '<TR><TD>N</TD><TD>C<sub>i</sub></TD> <TD>S<sub>E</sub></TD> <TD>S<sub>D</sub></TD> </TR>\n' \
                        '<TR><TD COLSPAN="4"><FONT POINT-SIZE="20">{}</FONT></TD></TR>\n' \
                        '<TR><TD>w</TD> <TD>C<sub>A</sub></TD> <TD>V<sub>E</sub></TD> <TD>V<sub>D</sub></TD> </TR>\n'\
                        '</TABLE>>]\n' 
            key_string = f_string.format(wrapped_title)
            f_dot.write(key_string)
            f_dot.write('{rank= "min"; "Key"}\n')
            invis_link = Key.dot_group_link(self)
            f_dot.write(invis_link + '\n')
                    
               
                    

    def display_nodes(self):
        """Display all the nodes in the tree"""
        for n in self.nodes.values():
            n.display()





#TODO: Reduce the property set to what is currently needed.
#      We currently have old calculated properties
class ConceptNode():
    """A node to be placed in a concept tree"""
    def __init__(self, id, label="lab",parent = None, novelty = 0, positioning='',fill='', fixed_level=0):
        self.id = id
        self.node_label = label
        self.positioning = positioning
        self.fillcolor = fill
        self.parent = None # id of parent node
        self.children = {} # dictionary of child nodes, with node id as keys
        self.fixed_level = fixed_level
        self.level = fixed_level
        self.b_variety = 0. # branch variety
        self.e_variety = 0. # element variety
        self.d_variety = 0. # sum of descendant branch varieties
        self.p_variety = 0. # allocated parent variety
        self.w_variety = 0. # Within-level variety
        self.c_average = 0. # average contribution
        self.c_increment = 0. # contribution increment
        self.b_increment = 0 # total branch contribution increment
        self.a_variety = 0. # allocated variety
        self.c_novelty = novelty # concept (idea) novelty
        self.c_tec_novelty = 0. # concept tech novelty
        self.c_tac_novelty = 0. # concept tac novelty
        self.sb_novelty = 0. # set novelty for the branch
        self.se_novelty = 0. # set novelty for the element
        self.se_tec_novelty = 0. # set tech novelty for the element
        self.se_tac_novelty = 0. # set tac novelty for the element
        self.sd_novelty = 0. # sum of descendant set branch novelty
        self.sd_tec_novelty = 0. # sum of descendant set branch tech novelty
        self.sd_tac_novelty = 0. # sum of descendant set branch tac novelty
        self.space_width = 0. # design space width for this element
        self.n_space_width = 0. # novel design space width for this element
        self.d_n_space_width = 0. # sum of descendant novel design space widths
        self.row = -1 # Row position of the node
        self.child_span = [-1, -1] # row of first child, row of last child
        self.version = Version
        self.quality =  0. #idea quality
        if parent != None:
            self.set_parent(parent)

    def child_count(self):
        """Return the number of siblings in the family of the node"""
        return(len(self.children))

    def calc_novelty(self):
        #print ("In calc_novelty: {}".format(self.id))
        sid = self.id
        sum_child_novelty = 0.0
        sum_sq_child_novelty = 0.0
        if self.children == {}:
            if self.c_novelty==0.0 :
                self.c_novelty = 0.0 # default value if none is given
            return (self.c_novelty)
        else:
            novelty = 0.0
            for c in self.children.values():
                cnov = c.calc_novelty()
                if cnov > novelty:
                    novelty = cnov
            self.c_novelty = novelty
            return(self.c_novelty)

    def calc_set_novelty(self,beta):
        """Calculate the set novelty"""
        sum_child_set_novelty = 0.0
        sum_cb_inc = 0.0
        if self.children == {}:
            self.sd_novelty = 0.0
        else:
            sid = self.id
            for c in self.children.values():
                sum_child_set_novelty += c.calc_set_novelty(beta) # recursive call
                sum_cb_inc += c.b_increment
            self.sd_novelty = sum_child_set_novelty
        if self.parent == None:
            self.se_novelty = self.c_novelty * self.e_variety # no parent, so no siblings, so VI=VE
        else:
            novelty_list = []
            for s in self.parent.children.values():
                novelty_list.append(s.c_novelty)
            novelty_list.sort(reverse=True)
            sib_index = novelty_list.index(self.c_novelty)
            self.c_increment = 1+beta ** sib_index
            i = sib_index + 1
            while i < len(novelty_list):
                if novelty_list[i] == self.c_novelty:
                    self.c_increment += 1 + beta ** i
                    i += 1
                else:
                    break
            self.c_increment /= (i-sib_index)
            weighted_inc = self.c_increment * self.weighting()
            self.b_increment = sum_cb_inc + weighted_inc 
            self.se_novelty = self.c_novelty * weighted_inc
        self.sb_novelty = self.se_novelty + self.sd_novelty
        return(self.sb_novelty)

    def branch_variety(self,beta):
        """Calculate and return the branch variety, including calculating
            descendant, element, and branch variety for all children."""
        self.d_variety = 0
        if self.parent == None:
            self.c_average = 2 # no siblings if no parent
        ch_sib_variety = self.children_variety(beta)
        self.e_variety = self.c_average * self.weighting()
        for ch in self.children.values():
            ch.c_average = ch_sib_variety 
            self.d_variety += ch.branch_variety(beta)
        self.b_variety = self.d_variety + self.e_variety
        return(self.b_variety)

# We no longer use allocated variety.  Space_width is now descendant variety + element variety = branch variety
# We could eliminate this method if we are sure we want to eliminate allocated variety.
# Note that space_width and b_variety are the same; we can eliminate space_width, I think.
    def allocated_variety(self):
        """ Calculate and return the parent and allocated variety for a node,
            and follow the tree down to calculate the parent and allocated variety
            for all children.  Depends on branch variety being correct.
            Also sets space_width."""
        if self.parent == None:
            self.p_variety = 0 # parent variety is obsolete; should we eliminate it?
        else:
            self.p_variety = self.parent.a_variety / self.parent.child_count()
        self.a_variety = self.p_variety + self.e_variety # allocated variety is obsolete; should we eliminate?  It's not wrong
        self.space_width = self.d_variety + self.e_variety
        for c in self.children.values():
            c.allocated_variety()
        return self.a_variety
            

    def children_variety(self,beta):
        """Return the sibling variety for the children (does not save it)"""
        sibcount = self.child_count()
        c_average = ConceptNode.average_variety(sibcount, beta)
        return c_average

    def average_variety(sibcount, beta):
        """Return the average variety for a group of sibcount siblings"""
        if sibcount == 0:
            c_average = 0
        else:
            c_average =  1 + ((beta ** sibcount - 1) /
                    (sibcount * (beta - 1)))
        return c_average
        
    

 # Warning -- ugly hack.  Need to find a better way to do it
    def weighting(self):
        """Return the weighting value for sibling variety"""
        return(ConceptNode.level_weighting(self.level))

    def level_weighting(level):
        """Return the weighting value for level"""
        if level == 4:
            return(0) # Objectives have zero weighting
        else:
            return (2 ** (level - 1))
        


    def set_parent(self, parent):
        """Assign parent to be the parent of the current node"""
        if self.parent != None:  # we have to eliminate self from the current parent
            self.parent.drop_child(self)
        self.parent = parent
        parent.add_child(self)
        #if parent.child_count() > 1:
        #    self.level = parent.level - 1
        #else:
        #    parent.level = self.level + 1
        #self.dirty = Truetree
        #parent.dirty = True
        #parent.clean_tree(beta)

    def calc_variety(self, beta):
        """Calculate the variety of the whole tree"""
        # TODO -- fix this code or eliminate it
        # NOT YET RIGHT -- DESCENDANT VARIETY IS GOOD< BUT WE DON"T BUILD UP BRANCH RIGHT, I THINK
        self.descendant_variety(beta) # sets the children's level
        self.sibling_variety(beta)
        self.branch_variety(beta)
        self.parent_variety(beta)
        self.allocated_variety(beta)
        #self.dirty = False

    def add_child(self, ch):
        """Add ch as a child"""
        self.children[ch.id] = ch
        #self.dirty = True

    def drop_child(self, ch):
        """Remove ch as a child"""
        del self.children[ch.id]
 #       self.dirty = True

    def child_level(self):
        """Set and return the tree level of the node based on the levels of the children"""
        # Both set and return the level
        if self.children == {}:
            self.level = 1
        else:
            self.level = 1 + max(c.child_level() for c in self.children.values())
        if self.fixed_level != 0:
            self.level = self.fixed_level 
        return self.level

    def parent_level(self):
        """Set and return the tree level of the node based on the parent, and
            follow changes down the tree if needed """
        if self.parent != None:
            if self.level != self.parent.level - 1 :
                self.level = self.parent.level -1
                if self.children != {}:
                    for c in self.children.values():
                        c.parent_level()
        if self.fixed_level != 0:
            self.level = self.fixed_level
        return self.level

    def calc_row_pos(self, first_row):
        """Calculate the row position for a node based on the row positions for the children"""
        # This is a recursive call
        temp_row = first_row
        min_row = 99999 #  big number, assumed to be the maximum row count
        max_row = 0
        if self.children == {}:
            self.row = first_row
            min_row = self.row
            max_row = self.row
            last_row = first_row
        else:
            for c in self.children.values():
                last_row = c.calc_row_pos(temp_row)
                if c.row < min_row:
                    min_row = c.row
                if c.row > max_row:
                    max_row = c.row
                temp_row = last_row + 1
            self.row = (min_row + max_row)//2
            self.child_span = [min_row, max_row]
        return(last_row)

    def display_subtree(self,max_level):
        """Display a subtree starting at the given node in a tree having max_level"""
        self.display_node(max_level)
        for ch in self.children.values():
            ch.display_subtree(max_level)

    def display(self):
        """Display a node.  This function is not yet complete"""
        format_str = 'ID: {} row: {}  child_span: {}'
        print (format_str.format(self.id, self.row, self.child_span))

#TODO: Make the tree printout work properly with multiple top-level nodes (recognize nodes with no parent)
    def display_node(self, max_level):
        """Display node information in the context of a tree having max_level"""
        format_str = '  '
        for i in range(1,max_level-self.level):
            if self.parent == None:
                format_str += '  '
            else:
                format_str += '| '
        if self.parent == None:
           format_str += '  '
        else:
            format_str += '|-'
        format_str += 'Label:{}, {} SN:{: .2f}, CN:{: .2f},'
        format_str += 'VB:{: .2f}, '
        format_str += 'CA:{: .2f}, VD:{: .2f}, l:{}, w:{}'
        print (format_str.format(self.node_label, self.id, self.sb_novelty, self.c_novelty, 
                                self.b_variety, 
                                self.c_average, self.d_variety, self.level, self.weighting()))

    def design_space_representation (self, kind = "DSI", s_min = 0, level_offset = -0.5, base_level = 20):
        """Create a TikZ design space representation of the node and and call recursively for the children
kind can be DSI (design space increment), NDSI (novel design space increment), or Novelty (set novelty).
Returns an array containing the maximum x coordinate of the design space and the TikZ string for the design space.
"""
        dsr = ""
        if kind == "DSI":
            space = self.b_variety
        elif kind == "NDSI" :
            space = self.b_increment #Use increment, instead of average 
        elif kind == "Novelty":
            space = self.sb_novelty
        else:
            print ("Invalid value for kind: '{}'; should be 'DSI', 'NDSI', or 'Novelty'".format (kind))
            return ([s_min,''])
        dsr = "\\draw [fill = {}] ({}, {: .2f}) rectangle ( {}, {: .2f}); \n".format(self.fillcolor, level_offset * self.level, s_min, base_level, s_min + space)
        dsr += "\\node [anchor = north west] at  ({: .2f},{: .2f}) {{ {} }};\n".format(level_offset * self.level , s_min + space , self.id)
        c_smin = s_min
        for c in self.children.values():
            cdsr = c.design_space_representation(kind, c_smin, level_offset, base_level)
            c_smin = cdsr[0]
            dsr += cdsr[1]
        return [s_min + space, dsr]
        
        
    def dot_node(self, twidth, plain=False):
        """ Create a node in dot tree format """
        #Background colors as a function of level
        colordict = {
            1: "lightpink1",
            2: "palegreen1",
            3: "lightgoldenrod1",
            4: "lightskyblue1"
            }
        mycolor = colordict.get(self.level, "white")
        orig_id = self.id #for debugging purposes
        myTitle = ConceptNode.dot_cleaned_string(self.id)
        wrapped_title = ConceptNode.dot_wrapped_string(myTitle, twidth)
        #print (self.id + ' ' + wrapped_title + ' ' + str(twidth) + str(len(self.id)))
        if plain:
            f_string = \
                        '"{}" [shape=none, margin = 0, group = "group{}", label=<\n' \
                        '<TABLE BORDER = "0" CELLBORDER = "1" bgcolor = "{}" cellspacing = "0">\n' \
                        '<TR><TD COLSPAN="4"><FONT POINT-SIZE="20">{}</FONT></TD></TR>\n' \
                        '</TABLE>>]\n' 
            node_string = f_string.format(myTitle, self.row,mycolor, wrapped_title)
        else:
            f_string = \
                        '"{}" [shape=none, margin = 0, group = "group{}", label=<\n' \
                        '<TABLE BORDER = "0" CELLBORDER = "1" bgcolor = "{}" cellspacing = "0">\n' \
                        '<TR><TD> {:.2f} </TD> <TD> {:.2f} </TD> <TD> {:.2f} </TD> <TD> {:.2f} </TD> </TR>\n' \
                        '<TR><TD COLSPAN="4"><FONT POINT-SIZE="20">{}</FONT></TD></TR>\n' \
                        '<TR><TD> {} </TD> <TD> {:.2f} </TD> <TD> {:.2f} </TD> <TD> {:.2f} </TD> </TR>\n'\
                        '</TABLE>>]\n' 
            node_string = f_string.format(myTitle,self.row,
                                          mycolor,
                                          self.c_novelty, self.c_increment, self.se_novelty, self.sd_novelty,
                                          wrapped_title,
                                          self.weighting(), self.c_average, self.e_variety, self.d_variety)
        return(node_string)

    def dot_cleaned_string(in_str):
        """ Return a cleaned string (with special characters replaced by harmless ones) """
        # Substitutions:
        # & -> and
        # + -> and
        # " -> '
        # -> -> to
        # > to over
        
        out_str = in_str
        pat1 = re.compile('&')
        pat2 = re.compile('\+')
        pat3 = re.compile ('"')
        pat4 = re.compile ('->')
        pat5 = re.compile ('>')
      
        p1_str = pat1.sub('and',out_str)
        p2_str = pat2.sub('and',p1_str)
        p3_str = pat3.sub("'", p2_str)
        p4_str = pat4.sub('to', p3_str)
        out_str = pat5.sub('over', p4_str)
    
        return(out_str)
    
    def dot_wrapped_string(in_str, numch):
        """ Return a wrapped string with a maximum width of numch """
        last_wrap = 0
        last_space = 0
        out_str = ""
        if in_str.find(' ', last_wrap + last_space) <= 0:
            #one-word id
            return(in_str)
        while in_str.find(' ', last_wrap + last_space) > 0:
            while (((j := in_str.find(' ',last_wrap + last_space+1)) > 0) and (j - last_wrap <= numch)):
                last_space = j-last_wrap
            out_str += in_str[last_wrap:last_wrap + last_space]
            if j > 0 or len(in_str) - last_wrap > numch:
                out_str += '<br/>'
            last_wrap = last_wrap + last_space + 1
            last_space = 0
            if j<0:
                out_str += ' ' + in_str[last_wrap:len(in_str)]
                last_wrap = len(in_str)
                last_space = 0
        return(out_str)
                

    def dot_children(self):
        """ Write the links to the children, in dot tree format """
        tree_string = ""
        for  c in self.children.values():
            tree_string += '"{}" -- "{}" [headport = "w"]\n'.format(ConceptNode.dot_cleaned_string(self.id), \
                                                                    ConceptNode.dot_cleaned_string(c.id))
        return(tree_string)

    def dot_group_link(self, tree):
        """Write an invisible link to a group member"""
        row = self.row
        if self.children == {}:
            return ('not needed') # no children, so no invisible link needed
        for c in self.children.values():
            if c.row == row:
                return ('not needed') # row is part of children, so no invisible link needed
        for n in tree.nodes.values():
            if n != self and n.parent != self and n.row == self.row:
                return ('"{}" -- "{}" [style=invis]\n'.format(ConceptNode.dot_cleaned_string(self.id), ConceptNode.dot_cleaned_string(n.id)))
        return ('not found')
        
        
        




# Testing Data
#nodeOne = ConceptNode('Node 1','n1')
#nodeTwo = ConceptNode('Node 2','n2')
#nodeThree = ConceptNode('Node 3','n3')
#myTree = ConceptTree('Tree 1')
#myTree.add_node(nodeOne)
#myTree.add_node(nodeTwo)
#myTree.add_node(nodeThree)
#myTree.add_node(ConceptNode('Node 4', parent=nodeTwo))
#nodeFive = myTree.add_node(ConceptNode('Node 5','n5'))
#nodeSix = myTree.add_node(ConceptNode('Node 6',parent=nodeFive, label='n6'))
#myTree.top_nodes()
#nodeThree.set_parent(nodeOne)
#nodeTwo.set_parent(nodeOne)
#nodeOne.clean_tree(0.7)


treeTwo = ConceptTree('TreeTwo')
Ofill = "blue!20"
Pfill = "yellow!50"
Efill = "violet!20"
Dfill = "red!20"
O1 = treeTwo.add_node(ConceptNode('Objective 1','O1', fixed_level = 4, fill = Ofill, positioning = "left = 6mm of P1, yshift = 3cm"))
P1 = treeTwo.add_node(ConceptNode('Principle 1', 'P1', O1, fill=Pfill,positioning = "left = of E1"))
P2 = treeTwo.add_node(ConceptNode('Principle 2', 'P2', O1, fill = Pfill, positioning = "left = of E3"))
E1 = treeTwo.add_node(ConceptNode('Embodiment 1', 'E1', P1, fill = Efill, positioning = "left = of D2"))
E2 = treeTwo.add_node(ConceptNode('Embodiment 2', 'E2', P1, fill = Efill, positioning = "left = of D4"))
E3 = treeTwo.add_node(ConceptNode('Embodiment 3', 'E3', P2, fill = Efill, positioning = "left = of D6"))
D1 = treeTwo.add_node(ConceptNode('Detail 1', 'D1', E1, fill = Dfill))
D2 = treeTwo.add_node(ConceptNode('Detail 2', 'D2', E1, fill = Dfill, positioning = "above = of D1"))
D3 = treeTwo.add_node(ConceptNode('Detail 3', 'D3', E1, fill = Dfill, positioning = "above = of D2"))
D4 = treeTwo.add_node(ConceptNode('Detail 4', 'D4', E2, fill = Dfill, positioning = "above = of D3"))
D5 = treeTwo.add_node(ConceptNode('Detail 5', 'D5', E2, fill = Dfill, positioning = "above = of D4"))
D6 = treeTwo.add_node(ConceptNode('Detail 6', 'D6', E3, fill = Dfill, positioning = "above = of D5"))

D1.c_novelty = 0.5
D2.c_novelty = 1
D3.c_novelty = 2
D4.c_novelty = 0.5
D5.c_novelty = 0.5
D6.c_novelty = 0.5

treeTwo.calc_variety()
treeTwo.calc_novelty()
treeTwo.calc_row_positions()
O1 = treeTwo.nodes['Objective 1']
O1.dot_group_link(treeTwo)


treeTwo.save_file('TwoJson.txt')
treeTwo.save_latex_standalone()
treeTwo.save_latex_plain_standalone()
treeTwo.save_latex_novelty_standalone()
treeTwo.save_latex_design_space_standalone()
treeTwo.save_latex_novel_design_space_standalone()
treeTwo.save_dot_tree()
#nov = treeTwo.create_ds_representation('Novelty')
#ds = treeTwo.create_ds_representation('DSI')
#nds = treeTwo.create_ds_representation('NDSI')

#treeThreeB.export_latex(stdout)
#treeThree = ConceptTree('Tree3')
#Ofill = "blue!20"
#Pfill = "yellow!50"
#Efill = "green!20"
#Dfill = "red!20"
#O1 = treeThree.add_node(ConceptNode('Objective 1','o1', fixed_level = 4, fill = Ofill, positioning = "above = 6mm of p1, xshift=2.7cm"))
#P1 = treeThree.add_node(ConceptNode('Principle 1', 'p1', O1, fixed_level = 3, fill=Pfill,positioning = "above = of e1, xshift=17mm"))
#P2 = treeThree.add_node(ConceptNode('Principle 2', 'p2', O1, fixed_level = 3, fill = Pfill, positioning = "above = of e3"))
#E1 = treeThree.add_node(ConceptNode('Embodiment 1', 'e1', P1, fixed_level = 2, fill = Efill, positioning = "above = of d2"))
#E2 = treeThree.add_node(ConceptNode('Embodiment 2', 'e2', P1, fixed_level = 2, fill = Efill, positioning = "above = of d4, xshift=8.5mm"))
#E3 = treeThree.add_node(ConceptNode('Embodiment 3', 'e3', P2, fixed_level = 2, fill = Efill, positioning = "above = of d6"))
#D1 = treeThree.add_node(ConceptNode('Detail 1', 'd1', E1, fixed_level = 1, fill = Dfill))
#D2 = treeThree.add_node(ConceptNode('Detail 2', 'd2', E1, fixed_level = 1, fill = Dfill, positioning = "right = of d1"))
#D3 = treeThree.add_node(ConceptNode('Detail 3', 'd3', E1, fixed_level = 1, fill = Dfill, positioning = "right = of d2"))
#D4 = treeThree.add_node(ConceptNode('Detail 4', 'd4', E2, fixed_level = 1, fill = Dfill, positioning = "right = of d3"))
#D5 = treeThree.add_node(ConceptNode('Detail 5', 'd5', E2, fixed_level = 1, fill = Dfill, positioning = "right = of d4"))
#D6 = treeThree.add_node(ConceptNode('Detail 6', 'd6', E3, fixed_level = 1, fill = Dfill, positioning = "right = of d5"))
#D7 = treeThree.add_node(ConceptNode('Detail 7', 'd7', fixed_level = 1, fill = Dfill, positioning = "right = of d6"))
#P3 = treeThree.add_node(ConceptNode('Principle 3', 'p3',  fixed_level = 3, fill = Pfill, positioning = "above = of e4"))
#E4 = treeThree.add_node(ConceptNode('Embodiment 4', 'e4', P3, fixed_level = 2, fill = Efill, positioning = "above = of d7, xshift = 8.5mm"))


#D1.c_novelty = 0.1

#D2.c_novelty = 1
#D3.c_novelty = 2
#D4.c_novelty = 0.5
#D5.c_novelty = 0.5
#D6.c_novelty = 0.5
#D7.c_novelty = 2
#E4.c_novelty = 1.5


#treeThree.calc_novelty()
#treeThree.save_latex()

#treeFour = copy.deepcopy(treeThree)
#treeFour.id = 'Tree4'
#treeFour.remove_node(treeFour.nodes['Detail 1'])
#t4d2 = treeFour.nodes['Detail 2']
#t4e1 = treeFour.nodes['Embodiment 1']
#t4d2.positioning = ''
#treeFour.calc_novelty()
##treeFour.save_latex()
#t4d2.calc_novelty()

#sTree=ConceptTree('SimpleTree')
#fill = "blue!20"
#Pfill = "yellow!50"
#Efill = "green!20"
#Dfill = "red!20"
#STO1 = sTree.add_node(ConceptNode('Objective 1','o1', fixed_level = 4, fill = Ofill, positioning = "above = of p1, "))
#STP1 = sTree.add_node(ConceptNode('Principle 1', 'p1', STO1, fixed_level = 3, fill=Pfill,positioning = "above = of e1,"))
#STE1 = sTree.add_node(ConceptNode('Embodiment 1', 'e1', STP1, fixed_level = 2, fill = Efill, positioning = "above = of d2"))
#STD1 = sTree.add_node(ConceptNode('Detail 1', 'd1', STE1, fixed_level = 1, fill = Dfill))
#STD2 = sTree.add_node(ConceptNode('Detail 2', 'd2', STE1, fixed_level = 1, fill = Dfill, positioning = "right = of d1"))
#STD3 = sTree.add_node(ConceptNode('Detail 3', 'd3', STE1, fixed_level = 1, fill = Dfill, positioning = "right = of d2"))

#STD1.c_novelty = 0.5
#STD2.c_novelty = 0.25
#STD3.c_novelty = 1.0

#sTree.calc_novelty()
#sTree.save_latex()
#sTree.save_latex_standalone()
