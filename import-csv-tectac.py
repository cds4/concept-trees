import csv
import sys
from ConceptTreeTecTac import *

myVersion = '2.3'


TreeName = 'Team10' # File name for exported latex files
ImportFile = 'Team10.csv' # file name for csv to be imported
ObjectiveName = 'Objective 1' # Design Objective (not in tree)
KeyPositioning = "above = 5cm of O1" #TikZ positioning command for Variety/Novelty key

def read_ideas(f_name):
    entry_list = []
    infile = open(f_name, 'r')
    csv_reader = csv.DictReader(infile)
    for row in csv_reader:
        entry_list.append(dict(row))
    infile.close()
    return(entry_list)

def new_element(element_name, element_parent):
    """Create a new element with the given name and text.  The element will
have blank values for all of the required element properties."""
    element_dict = {'Name':element_name, 'Parent': element_parent, 'Novelty':'',
                          'Tac_Novelty':'', 'Tech_Novelty':'', 'Quality':'',
                          'Effectiveness':'','Feasibility':'',
                          'Has_Tactics':'','Has_Technology':''}
    return(element_dict)

def get_principle_code(p_text):
    """Find the principle code for a principle having the text p_text.
If the principle is found, just return the code.
If the principle is not found, add a new principle to the principles list,
and return the code """
    global principle_count
    if p_text == "":
        return ("") # No principle to look for
    for p in principles.items(): 
        if p[1]['Name'] == p_text:
            return (p[0])
    # not found, so add
    principle_count += 1
    p_code = 'P' + str(principle_count)
    principles[p_code] = new_element(p_text,'')
    #print(p_code)
    return(p_code)
    

def get_embodiment_code(e_text, p_text):
    """Find the embodiment code for an embodiment having the text e_text.
If the embodiment is found, just return the code.
If the embodiment is not found, add a new embodiment to the embodiments list,
get the principle code to add to the embodiment (if the p_text is not empty),
and return the embodiment code """
    global embodiment_count
    #print ('In get_embodiment_code\n  e_text ' + e_text + '\n  p_text ' + p_text)
    if e_text == "":
        return("") # no embodiment to look for
    for e in embodiments.items():
        #print ('e ' + e[0] + ': Name ' + e[1]['Name'])
        if e[1]['Name'] == e_text:
            return (e[0])
    # not found, so add
    embodiment_count += 1
    e_code = 'E' + str(embodiment_count)
    p_code = get_principle_code(p_text)
    embodiments[e_code] = new_element(e_text, p_code)
    #print(e_code)
    return(e_code)

def get_detail_code (d_text, e_text, p_text):
    """Find the detail code for a detail having the text d_text.
If the detail is found, print a warning and don't add a new one.
If the detail is not found, add a new detail to the details list,
get the embodiment code to add to the detail (if the e_text is not empty),
and return the detail code """
    global detail_count
    #print ('in get_detail_code \n' + d_text + ' \n' + e_text + '\n ' + p_text)
    if d_text == "":
        return ("")
    for d in details.items():
        #print (d)
        if d[1]['Name'] == d_text:
            return (d[0])
    # not found, so add
    detail_count += 1
    d_code = 'D' + str(detail_count)
    e_code = get_embodiment_code(e_text, p_text)
    details[d_code] = new_element(d_text, e_code)
    #print(d_code)
    return(d_code)
    
arg_count = len(sys.argv)-1

if arg_count == 0:
    print ("No arguments found.  Using defaults in import-csv.py")
if arg_count == 1:
    ImportFile = sys.argv[1]
    dot_pos = ImportFile.index('.',0)
    TreeName = ImportFile[:dot_pos]
    # Use defaults for rest
    #ObjectiveName = TreeName
    #KeyPositioning = "above = 5cm of O1" #TikZ positioning command for Variety/Novelty key
if arg_count == 2:
    ImportFile = sys.argv[1]
    TreeName = sys.argv[2]
    #Use defaults for rest
    #ObjectiveName = TreeName
    #KeyPositioning = "above = 5cm of O1" #TikZ positioning command for Variety/Novelty key
if arg_count == 3:
    ImportFile = sys.argv[1]
    TreeName = sys.argv[2]
    ObjectiveName = sys.argv[3]
    #Use Defaults for rest
    #KeyPositioning = "above = 5cm of O1" #TikZ positioning command for Variety/Novelty key
if arg_count == 4:
    ImportFile = sys.argv[1]
    TreeName = sys.argv[2]
    ObjectiveName = sys.argv[3]
    KeyPositioning = argv[4]
    
ideas = {}
principles = {}
embodiments = {}
details = {}
principle_count = 0
embodiment_count = 0
detail_count = 0
    
    
    

imported_ideas = read_ideas(ImportFile)



Ofill = "blue!20"
Pfill = "yellow!50"
Efill = "green!20"
Dfill = "red!20"

current_principle = ""
current_embodiment = ""
current_detail = ""

for id in imported_ideas:
    idea = id['Idea']
    p_no = id['Principle_No']
    try:
        idea_code = id['Code']
    except:
        idea_code = ''
    try:
        idea_novelty = id['Novelty']
    except:
        idea_novelty = ''
    try:
        idea_tech_novelty = id['Tech_Novelty']
    except:
        idea_tech_novelty = ''
    try:
        idea_tac_novelty = id['Tac_Novelty']
    except:
        idea_tac_novelty = ''
    try:
        idea_quality = id['Quality']
    except:
        idea_quality = ''
    try:
        idea_feasibility = id['Feasibility']
    except:
        idea_feasibility = ''
    try:
        idea_effectiveness = id['Effectiveness']
    except:
        idea_effectiveness = ''
    try:
        idea_tactics = id['Tactics']
        if idea_tactics == 'N':
            idea_tactics = ''
    except:
        idea_tactics = ''
    try:
        idea_technology = id['Technology']
        if idea_technology == 'N':
            idea_technology = ''
    except:
        idea_technology = ''

    if p_no != '0':
        element = ''
        if id['Detail']!= "":
            d_code = get_detail_code(id['Detail'],id['Embodiment'],id['Principle'])
            element = details[d_code]
        elif id['Embodiment']!= "":
            e_code = get_embodiment_code(id['Embodiment'],id['Principle'])
            element = embodiments[e_code]
        elif id['Principle'] != "":
            p_code = get_principle_code(id['Principle'])
            element = embodiments[p_code]
        if element != '':
            element['Novelty'] = idea_novelty
            element['Tech_Novelty'] = idea_tech_novelty
            element['Tac_Novelty'] = idea_tac_novelty
            element['Quality'] = idea_quality
            element['Feasibility'] = idea_feasibility
            element['Effectiveness'] = idea_effectiveness
            element['Has_Tactics'] = idea_tactics
            element['Has_Technology'] = idea_technology

        if idea_code!="":
            ideas[idea_code] = {'Level':id['Level'], 'Idea':id['Idea']}
       
treeVar = 'tree'+TreeName
vars()[treeVar]=ConceptTree(TreeName)

o_ref = int(len(principles)/2)
o_ref_node = 'P'+ str(o_ref)

O1 = vars()[treeVar].add_node(ConceptNode(ObjectiveName,'O1',fixed_level=4, fill=Ofill, \
                                          positioning = 'left = of ' + o_ref_node + ','))                      
def set_values (source, dest):
    if source['Tech_Novelty'] != "":
        dest.set_concept_novelty('Technology',float(source['Tech_Novelty']))
    if source['Tac_Novelty'] != "":
        dest.set_concept_novelty('Tactics',float(source['Tac_Novelty']))
    # set Novelty after Tech_Novelty and Tac_Novelty so it won't be overridden by calcs
    if source['Novelty'] != "": 
        dest.set_concept_novelty('Total',float(source['Novelty']))
    if source['Quality'] != "":
        dest.quality = float(source['Quality'])
    if source['Effectiveness'] != "":
       dest.effectiveness = float(source['Effectiveness'])
    if source['Feasibility'] != "":
        dest.feasibility = float(source['Feasibility'])
    dest.has_content['Tactics'] = source['Has_Tactics'] != ''
    dest.has_content['Technology'] = source['Has_Technology'] != ''

for p in principles.items():
    id = p[0]
    name = p[1]['Name']
    parent = p[1]['Parent']
    vars()[id] = vars()[treeVar].add_node(ConceptNode(name,id,fixed_level=3, fill=Pfill))
    set_values(p[1],vars()[id])
    if parent != "":
        vars()[id].set_parent(vars()[parent])
    else:
        vars()[id].set_parent(O1)

for e in embodiments.items():
    id = e[0]
    name = e[1]['Name']
    parent = e[1]['Parent']
    vars()[id] = vars()[treeVar].add_node(ConceptNode(name,id,fixed_level=2, fill=Efill))
    set_values(e[1],vars()[id])
    if parent != "":
        vars()[id].set_parent(vars()[parent])

for d in details.items():
    id = d[0]
    name = d[1]['Name']
    parent = d[1]['Parent']
    vars()[id] = vars()[treeVar].add_node(ConceptNode(name,id,fixed_level=1, fill=Dfill))
    set_values(d[1],vars()[id])
    if parent != "":
        vars()[id].set_parent(vars()[parent])
           
vars()[treeVar].set_content() #establish has_content settings based on tree relationships

p_count = len(principles)
e_count = len(embodiments)
d_count = len(details)

max_count = max(p_count, e_count, d_count)

#TODO:  Get rid of positioning -- no longer using LaTeX

ordered_dict={}
index = 0
if max_count == p_count: # more principles than anything
    for p in range(1,p_count+1):
        id = 'P'  + str(p)
        ref_id = 'P' + str(p-1)
        if p > 1:
            vars()[id].positioning = 'below = of ' + ref_id + ','
        ordered_dict[index] = vars()[id]
        index += 1
    for e in range(1, e_count+1):
        curr_id = 'E' + str(e)
        prev_id = 'E' + str(e-1)
        if e == 1:
            vars()[curr_id].positioning = 'right = of P1'
        else:
            if vars()[curr_id].parent != None:
                parent_id=vars()[curr_id].parent.node_label
                prev_parent_id = vars()[prev_id].parent.node_label
                parent_id_num = int(parent_id[1:])
                if prev_parent_id != parent_id:
                    vars()[id].positioning = 'right = of P' + str(e) + ','
                else:
                    vars()[id].positioning = 'below = of ' + prev_id +','
        ordered_dict[index] = vars()[id]
        index += 1
        
    for d in range(1,d_count+1):
        curr_id = 'D' + str(d)
        prev_id = 'D' + str(d-1)
        if d == 1:
           vars()[curr_id].positioning = 'right = of E1,'
        else:
            if vars()[curr_id].parent != None:
                parent_id=vars()[curr_id].parent.node_label
                prev_parent_id = vars()[prev_id].parent.node_label
                parent_id_num = int(parent_id[1:])
                if prev_parent_id != parent_id:
                    vars()[curr_id].positioning = 'right = of E' + str(d) + ','
                else:
                    vars()[curr_id].positioning = 'below = of ' + prev_id + ','
        ordered_dict[index] = vars()[id]
        index += 1
                   
elif max_count == e_count: # more embodiments than anything
    for e in range( 1,e_count+1):
        id = 'E'  + str(e)
        prev_id = 'E' + str(e-1)
        # position each embodiment below the previous one
        if e > 1:
            vars()[id].positioning = 'below = of ' + prev_id +','
        ordered_dict[index] = vars()[id]
        index += 1
        # position each principle to the left of one of its children
        if vars()[id].parent != None :
            if vars()[id].parent.positioning == "" :
                vars()[id].parent.positioning = 'left = of ' + id +','
    # add the parents to the ordered array
    for p in range(1, p_count+1):
        id = 'P' + str(p)
        ordered_dict[index] = vars()[id]
        index += 1
    for d in range(1, d_count+1):
        id = 'D' + str(d)
        # position each detail to either the right of its parent or below its siblings
        curr_id = 'D' + str(d)
        prev_id = 'D' + str(d-1)
        if d == 1:
            vars()[curr_id].positioning = 'right = of E1,'
        else:
            if vars()[curr_id].parent != None:
                parent_id=vars()[curr_id].parent.node_label
                prev_parent_id = vars()[prev_id].parent.node_label
                parent_id_num = int(parent_id[1:])
                if prev_parent_id != parent_id:
                    #if d >= int(parent_id[1:]): #check was insufficient -- need to know if family, not just detail
                        vars()[curr_id].positioning = 'right = of E' + str(d) + ','
                    #else:
                    #    vars()[curr_id].positioning = 'right = of ' + parent_id + ','
                else:
                    vars()[curr_id].positioning = 'below = of ' + prev_id + ','
        ordered_dict[index] = vars()[id]
        index += 1


elif max_count == d_count: #more details than anything
    for d in range( 1, d_count+1):
        id = 'D'  + str(d)
        ref_id = 'D' + str(d-1)
        if d > 1:
            vars()[id].positioning = 'below = of ' + ref_id + ','
        if vars()[id].parent != None:
            if vars()[id].parent.positioning == "" :
                vars()[id].parent.positioning = 'left = of ' + id + ','
        ordered_dict[index] = vars()[id]
        index += 1
    for e in range( 1 , e_count+1):
        id = 'E' + str(e)
        if vars()[id].parent != None:
            if vars()[id].parent.positioning == "" :
                vars()[id].parent.positioning = 'left = of ' + id + ','
        ordered_dict[index] = vars()[id]
        index += 1
    for p in range(1, p_count+1):
        id = 'P' + str(p)
        if vars()[id].parent != None:
            if vars()[id].parent.positioning == "" :
                vars()[id].parent.positioning = 'left = of ' + id + ','
        ordered_dict[index] = vars()[id]
        index += 1
        
ordered_dict[index] = O1

def display_variety_novelty_quality(myTree, kind = 'All'):
    if kind == 'All':
        for k in Novelty_Types:
            display_variety_novelty_quality_specific(myTree,k)
            print ('\n')
    else:
        display_variety_novelty_quality_specific(myTree,kind)

def display_variety_novelty_quality_specific(myTree, kind):
    print('Data generated by import-csv-tectac.py version {}\n' \
          '  and ConceptTreeTecTac.py version {}\n' \
          'For tree {}'.format(
              Version,
              myTree.version,
              myTree.id))
    v = myTree.calc_variety(kind)
    min_v = myTree.minimum_variety(kind)
    max_v = myTree.maximum_variety(kind)
    norm_v = (v - min_v)/(max_v - min_v)
    quan = myTree.calc_quantity(kind)
    qual = myTree.calc_quality(kind)
    myTree.calc_novelty(kind)
    nov = myTree.novelty[kind]
    disp_str = 'For {} novelty:\n' \
               ' Quantity: {}\n' \
               ' Variety : {:.2f}\n' \
               '    Minimum Variety: {:.2f}\n' \
               '    Maximum Variety: {:.2f}\n'  \
               '    Normalized Variety: {:.2f}\n'  \
               ' Set Novelty {:.2f}\n'  \
               ' Quality  {}\n'
    print(disp_str.format(kind, quan, v, min_v, max_v, norm_v, nov, qual))


vars()[treeVar].calc_row_positions()         
vars()[treeVar].save_dot_tree()         
vars()[treeVar].save_dot_tree(kind = 'Total')         
vars()[treeVar].save_dot_tree(kind = 'Technology')         
vars()[treeVar].save_dot_tree(kind = 'Tactics')         
vars()[treeVar].save_dot_tree_plain()

print (treeVar)
display_variety_novelty_quality(vars()[treeVar], 'All')

       
